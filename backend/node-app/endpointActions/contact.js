const emails = require('./email')

//Send reset email to user with magic link
exports.sendContactEmail = async (name, email, message) => { 
    return new Promise((resolve, reject) => {
        const templateFile = 'contactUsEmail.html';

        //Template replacements
        const replacements = {
            name: name,
            email: email,
            message: message,
        };

        const mailOptions = {
            from: 'BlockFade <mail@blockfade.com>',
            to: "andy.elick@gmail.com, mail@blockfade.com",
            subject: "User has contacted us",
        };

        //Send the email
        emails.sendEmail(templateFile, replacements, mailOptions)
            .then(() => {
                return resolve({success: 'Email has been sent to blockfade team' })
            })
            .catch(error => {
                console.log(error)
                throw reject(error)
        });
    });
}

