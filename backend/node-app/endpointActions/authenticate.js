const db = require('../db/connectDb');
const jwt = require('jsonwebtoken');

//Create Access & Refresh tokens
exports.createTokens = userInfo => {
    return new Promise((resolve, reject) => {
        const accessToken = jwt.sign(userInfo, process.env.JWT_ACCESS_TOKEN_SECRET, { expiresIn: '24h' })
        const refreshToken = jwt.sign(userInfo, process.env.JWT_REFRESH_TOKEN_SECRET)
        const userId = userInfo.userId

        //Save refresh token to Token table
        saveRefreshToken(userId, refreshToken)
            .then(() => {
                return resolve({
                    accessToken: accessToken,
                    refreshToken: refreshToken,
                })
            })
            .catch(error => {
                reject(error)
            });
    });
}

//Creates Access token
createAccessToken = userInfo => {
    const accessToken = jwt.sign(userInfo, process.env.JWT_ACCESS_TOKEN_SECRET, { expiresIn: '24h' })
    return { accessToken: accessToken }
}

//Check if user token is valid
exports.authenticateToken = (req, res, next) => {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]

    //If there is no authentication header
    if (token == null) {
        return res.status(401).send({
            error: 'No access token provided',
            reason: 'no token'
        })
    }

    //Check if token is correct
    jwt.verify(token, process.env.JWT_ACCESS_TOKEN_SECRET, (err, data) => {
        //If invalid token
        if (err) {
            return res.status(403).send({
                error: 'Access token is no longer valid',
                reason: 'expired',
            })
        }

        //Set user
        req.success = true
        req.data = data
        next()
    })
}

//Save the refresh token to the database
saveRefreshToken = (userId, refreshToken) => {
    return new Promise((resolve, reject) => {
        const query = 'INSERT INTO tokens (user_id, refresh_token, created_on) VALUES ($1, $2, NOW()::date)'

        //Save to Token table
        db.query(query, [userId, refreshToken], (err, req) => {
            if (err) {
                return reject({
                    error: err.message,
                    reason: 'database error',
                    statusCode: 500,
                })
            }

            return resolve({ success: 'Saved to database' })
        });
    });
}

//Check if refresh token is in database
checkRefreshToken = refreshToken => {
    return new Promise((resolve, reject) => {
        const query = "SELECT * FROM tokens WHERE refresh_token = $1";

        //Check database
        db.query(query, [refreshToken], (err, res) => {
            //If error
            if (err) {
                return reject({
                    error: err.message,
                    reason: 'database error',
                    statusCode: 500
                })
            }

            //Rows returned
            const numRows = res.rows.length

            //If no record of refresh token
            if (numRows != 1) {
                return reject({
                    error: 'Refresh token not found',
                    reason: 'expired refresh token',
                    statusCode: 401,
                });
            }

            return resolve(numRows)
        });
    });
}

//Refresh token 
exports.refreshToken = refreshToken => {

    return new Promise((resolve, reject) => {

        //Check for refresh token
        checkRefreshToken(refreshToken)
            .then(() => {
                //Check if refresh token is valid
                jwt.verify(refreshToken, process.env.JWT_REFRESH_TOKEN_SECRET, (err, user) => {
                    if (err) {
                        return reject({
                            error: err.message,
                            reason: 'invalid refresh token',
                            statusCode: 403,
                        })
                    }

                    //Useful user details to send back
                    const userInfo = {
                        userId: user.userId,
                        email: user.email,
                        firstName: user.first_name,
                        lastName: user.last_name
                    }

                    //Create new set of tokens
                    const accessToken = createAccessToken(userInfo)

                    return resolve(accessToken)
                })
            })
            .catch(error => {
                return reject(error)
            });

    });

}

//Delete refresh token
exports.deleteRefreshToken = refreshToken => {
    return new Promise((resolve, reject) => {
        const query = "DELETE FROM tokens where refresh_token = $1"

        //Delete from database
        db.query(query, [refreshToken], (err, res) => {
            //If error
            if (err) {
                return reject({
                    error: err.message,
                    reason: 'database error',
                    statusCode: 500
                })
            }

            resolve({
                success: "Deleted refresh key",
                statusCode: 204,
            })
        })
    })
}

//Delete all refresh tokens for a user
exports.deleteAllRefreshTokens = userId => {
    return new Promise((resolve, reject) => {
        const query = "DELETE FROM tokens where user_id = $1"

        //Delete from database
        db.query(query, [userId], (err, res) => {
            //If error
            if (err) {
                return reject({
                    error: err.message,
                    reason: 'database error',
                    statusCode: 500
                })
            }

            resolve({
                success: "Deleted all refresh keys for this user",
                statusCode: 204,
            })
        })
    })
}