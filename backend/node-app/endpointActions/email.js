const mailgun = require('mailgun-js')({apiKey: process.env.EMAIL_SENDER_API_KEY, domain: process.env.EMAIL_SENDER_DOMAIN});
const handlebars = require("handlebars");
const fs = require("fs")
const jwt = require('jsonwebtoken');
const db = require('../db/connectDb');

//Read the HTML files
readHTMLFile = (path, callback) => {
    fs.readFile(path, { encoding: 'utf-8' }, function (err, html) {
        if (err) {
            throw {
                error: err.message,
                statusCode: 500,
                reason: 'Error while reading HTML file'
            };
        }
        else {
            callback(null, html);
        }
    });
};

// Blank method for sending different emails
exports.sendEmail = (emailTemplate, replacements, mailOptions) => {
    return new Promise((resolve, reject) => {
        const emailTemplatePath = '../node-app/emailTemplates/' + emailTemplate;

        //Read the HTML file
        readHTMLFile(emailTemplatePath, function (err, html) {
            if (err) {
                return reject({
                    error: err,
                    statusCode: 500,
                    reason: 'Error reading email template'
                })
            }

            //Compile the HTML template
            const template = handlebars.compile(html);

            //Template the variables
            const htmlToSend = template(replacements);

            //Add template to the mailOptions
            mailOptions["html"] = htmlToSend

            //Send the email
            mailgun.messages().send(mailOptions, (error, body) => {
                if (error) {
                    return reject({
                        error: error,
                        statusCode: 500,
                        reason: 'Error sending email'
                    })
                }

                return resolve(true)
            });
        });
    });
}

//Send email with magic link
exports.sendConfirmationEmail = async (userId, userEmail, firstName) => {

    //Email template file
    const templateFile = 'confirmEmail.html';

    //Create magic link and save token in database
    const magicLink = await createMagicLink(userId);

    //Template replacements
    const replacements = {
        name: firstName,
        magicLink: magicLink
    };

    const mailOptions = {
        from: 'BlockFade <mail@blockfade.com>',
        to: userEmail,
        subject: "Confirm your email address",
    };

    //Send the email
    sendEmail(templateFile, replacements, mailOptions)
        .then(() => {
            return true
        })
        .catch(error => {
            console.log(error)
            return error
        });
}

//Generate the magic link
createMagicLink = userId => {
    return new Promise((resolve, reject) => {
        //Generate access token
        const magicToken = jwt.sign({ userId: userId }, process.env.JWT_ACCESS_TOKEN_SECRET, { expiresIn: '24hours' });

        //Save token
        saveMagicToken(userId, magicToken)
            .then(() => {
                //Magic link
                const magicLink = "https://www.blockfade.com/magic/confirmAccount?token=" + magicToken;
                resolve(magicLink)
            })
            .catch(error => {
                reject(error)
            });
    });
}

//Save the refresh token to the database
saveMagicToken = (userId, magicToken) => {
    return new Promise((resolve, reject) => {
        const query = 'INSERT INTO confirm_tokens (user_id, magic_token, created_on) VALUES ($1, $2, NOW()::date) ON CONFLICT (user_id) DO UPDATE SET magic_token = excluded.magic_token';

        //Save to confirm tokens table
        db.query(query, [userId, magicToken], (err, req) => {
            if (err) {
                reject({
                    error: err.message,
                    reason: 'database error',
                    statusCode: 500,
                })
            }

            return resolve({ success: 'Saved to database' })
        });
    });
}

//Delete magic token from confirmed tokens
exports.deleteMagicToken = userId => {
    return new Promise((resolve, reject) => {
        const query = "DELETE FROM confirm_tokens WHERE user_id = $1 RETURNING *";

        //Check confirm tokens table
        db.query(query, [userId], (err, res) => {
            if (err) {
                return reject({
                    error: err.message,
                    statusCode: err.statusCode,
                    reason: 'Database error'
                })
            }

            //Results from query
            const foundUser = res.rows;

            //If no account found
            if (foundUser.length > 0) {
                return resolve({ success: "user email confirmation token found and deleted" })
            } else {
                return reject({
                    error: "Account not found in confirm tokens",
                    statusCode: 409,
                    reason: "No confirm token"
                })
            }

        });
    });
}