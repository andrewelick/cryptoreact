const db = require('../db/connectDb');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const email = require('./email');

//Save reset password token to database
saveResetToken = (userId, resetToken) => {
    return new Promise((resolve, reject) => {
        const query = 'INSERT INTO reset_password_tokens (user_id, reset_password_token, created_on) VALUES ($1, $2, NOW()::date) ON CONFLICT (user_id) DO UPDATE SET reset_password_token = excluded.reset_password_token';
        
        //Save to confirm tokens table
        db.query(query, [userId, resetToken], (err, req) => {
            if (err) {
                return reject({
                    error: err.message,
                    reason: 'database error',
                    statusCode: 500,
                });
            }

            resolve({ success: 'Saved to database' })
        });
    });
}

//Delete reset password token
exports.deleteResetToken = userId => {
    return new Promise((resolve, reject) => {
        const query = 'DELETE FROM reset_password_tokens WHERE user_id = $1';

        //Delete from database
        db.query(query, [userId], (err, req) => {
            if (err) {
                return reject({
                    error: err.message,
                    reason: 'database error',
                    statusCode: 500,
                });
            }

            resolve(true);
        });
    });
}

//Update user password in database
exports.updatePassword = (userId, passwordVal) => {
    return new Promise((resolve, reject) => {
        //Salt for password hash
        const saltRounds = 8;

        //Hash the password
        bcrypt.hash(passwordVal, saltRounds, (err, hash) => {
            if (err) {
                return reject({
                    error: err,
                    statusCode: 500,
                    reason: 'Problem while hashing password',
                })
            }
            
            const query = 'UPDATE users SET password = $1 WHERE user_id = $2';

            //Delete from database
            db.query(query, [hash, userId], (err, req) => {
                if (err) {
                    return reject({
                        error: err.message,
                        reason: 'database error',
                        statusCode: 500,
                    });
                }

                resolve(true);
            });
        });
    });
}

//Check reset token in table
exports.checkResetToken = userId => {
    return new Promise((resolve, reject) => {
        const query = 'SELECT * FROM reset_password_tokens WHERE user_id = $1';

        //Delete from database
        db.query(query, [userId], (err, req) => {
            if (err) {
                return reject({
                    error: err.message,
                    reason: 'database error',
                    statusCode: 500,
                });
            }

            //If no token is found
            if (req.rowCount != 1) {
                return reject({
                    error: "This token has expired or has been used",
                    statusCode: 400,
                    reason: "No reset password token found"
                });
            }

            resolve(true);
        });
    });
}

//Create reset password token and store in database
createResetPasswordLink = userId => {
    return new Promise((resolve, reject) => {
        //Generate access token
        const resetToken = jwt.sign({ userId: userId }, process.env.JWT_ACCESS_TOKEN_SECRET, { expiresIn: '24h' });

        //Save to database
        saveResetToken(userId, resetToken)
            .then(() => {
                const magicLink = "https://blockfade.com/forgot?token=" + resetToken;
                resolve(magicLink)
            })
            .catch(error => {
                reject(error)
            })
    })
}

//Send reset email to user with magic link
exports.sendResetEmail = async (userId, userEmail) => { 
    //Create magic link and save token in database
    createResetPasswordLink(userId)
        .then(magicLink => {
            //Email template file
            const templateFile = 'resetPasswordEmail.html';

            //Template replacements
            const replacements = {
                magicLink: magicLink
            };

            const mailOptions = {
                from: 'BlockFade <mail@blockfade.com>',
                to: userEmail,
                subject: "Reset password request",
            };

            //Send the email
            email.sendEmail(templateFile, replacements, mailOptions)
                .then(() => {
                    return true
                })
                .catch(error => {
                    console.log(error)
                    throw error
            });
        })
        .catch(error => {
            return error
        });
}
