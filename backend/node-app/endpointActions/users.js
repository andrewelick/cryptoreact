const db = require('../db/connectDb');
const bcrypt = require('bcryptjs');
const sendEmail = require('./email.js');
const followingCoins = require('./followingCoins');

//Check if email is already in table
checkEmail = email => {
    return new Promise((resolve, reject) => {
        //Lowercase email
        email = email.toLowerCase();
        
        //Query and userInfo array
        const query = 'SELECT * FROM users WHERE email = $1'
        const userInfo = [email]

        //Check Users table
        db.query(query, userInfo, (err, res) => {
            if (err) {
                reject(err)
            }

            //Rows returned
            const numRows = res.rows.length

            if (numRows > 0) {
                reject({
                    statusCode: 403,
                    error: "Email already in use",
                })
            }

            resolve(res)
        });
    })
}

//Generate random userId
createRandomUserId = () => {
    return new Promise((resolve, reject) => {
        let userId = '';
        let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;

        for (var i = 0; i < 6; i++) {
            userId += characters.charAt(Math.floor(Math.random() * charactersLength));
        }

        resolve(userId)
    });
}

//Create a new user
exports.createUser = (email, password, firstName, lastName) => {
    return new Promise((resolve, reject) => {
        //Lowercase all chars in users name, this ensures we can make just first character capital
        firstName = firstName.toLowerCase()
        lastName = lastName.toLowerCase()

        //Quick formatting of user data
        email = email.toLowerCase();
        firstName = firstName.charAt(0).toUpperCase() + firstName.slice(1);
        lastName = lastName.charAt(0).toUpperCase() + lastName.slice(1);

        //Check if email is already in use
        checkEmail(email)
            .then(() => {
                //Create random userId
                createRandomUserId()
                    .then(userId => {

                        //Salt for password hash
                        const saltRounds = 8;

                        //Hash the password
                        bcrypt.hash(password, saltRounds, (err, hash) => {
                            if (err) {
                                reject(err)
                            }

                            //Query and userInfo array
                            const query = 'INSERT INTO users (user_id, email, password, first_name, last_name, created_on) VALUES ($1, $2, $3, $4, $5, NOW()::date)'
                            const userInfo = [userId, email, hash, firstName, lastName]

                            //Insert into the Users table
                            db.query(query, userInfo, (err, res) => {
                                if (err) {
                                    reject(err)
                                }

                                //Send confirmation email
                                sendEmail.sendConfirmationEmail(userId, email, firstName)

                                //Create following coins row
                                followingCoins.updateFollowingCoins(userId, []);

                                resolve({ success: 'Created user account' })
                            });
                        });
                    })
                    .catch(error => {
                        reject(error);
                    });
            })
            .catch(error => {
                reject(error)
            })
    });
}

//Update user email confirmation to true
exports.confirmEmailUser = userId => {
    return new Promise((resolve, reject) => {
        const query = "UPDATE users SET email_confirmed = 'true' WHERE user_id = $1";

        //Update user email confirmed
        db.query(query, [userId], (err, res) => {
            if (err) {
                reject({
                    error: err.message,
                    statusCode: err.statusCode,
                    reason: 'Database error'
                })
            }

            const rowCount = res.rowCount

            //If no account found
            if (rowCount != 1) {
                reject({
                    error: "Could not update users email confirmation status.",
                    statusCode: 404,
                    reason: "No user account found with that id"
                })
            }

            resolve({ success: true })
        });
    });
}

//Check if user is in database with given password
exports.loginUser = (email, password) => {
    return new Promise((resolve, reject) => {
        //Lowercase email
        email = email.toLowerCase();

        const query = "SELECT * FROM users WHERE email = $1"
        const loginInfo = [email]

        //Check users table
        db.query(query, loginInfo, (err, res) => {
            if (err) {
                reject(err)
            }

            //Results from query
            const foundUser = res.rows

            //No results found
            if (foundUser.length != 1) {
                reject({
                    error: "Account could not be found"
                })
            } else {
                //Password from account
                let hashedPassword = foundUser[0].password

                //Check if password is correct
                bcrypt.compare(password, hashedPassword)
                    .then(res => {
                        if (res == false) {
                            reject({
                                error: "Invalid email/password"
                            })
                        }

                        //Useful user details to send back
                        const userDetails = {
                            userId: foundUser[0].user_id,
                            email: foundUser[0].email,
                            firstName: foundUser[0].first_name,
                            lastName: foundUser[0].last_name,
                            emailConfirmed: foundUser[0].email_confirmed,
                        }

                        resolve(userDetails)
                    })
                    .catch(error => {
                        reject(error)
                    });
            }
        });
    });
}

//Get users id, name
exports.getUserInfo = email => {
    return new Promise((resolve, reject) => {
        //Lowercase email
        email = email.toLowerCase();

        const query = "SELECT * FROM users WHERE email = $1";

        db.query(query, [email], (err, res) => {
            if (err) {
                reject({
                    error: err.message,
                    statusCode: err.statusCode,
                    reason: 'Database error'
                })
            }

            //Count rows returned
            foundUser = res.rowCount

            if (foundUser != 1) {
                reject({
                    error: "Could not retrieve user information from database",
                    statusCode: 200,
                    reason: "No user found"
                })
            }

            resolve({ userDetails: res.rows })
        });
    })
}