const fetch = require('node-fetch')

exports.getNewsFeed = url => {

    return new Promise((resolve, reject) => {
        //Do not include this in commit
        const apiKey = process.env.CRYPTO_COMPARE_API_KEY;
        url = url + apiKey

        //Get news feed from CryptoCompare API
        fetch(url)
            .then(response => response.json())
            .then(news => {
                const newsData = news.Data

                resolve(newsData)
            })
            .catch(error => {
                reject(error)
            })
    });
};
