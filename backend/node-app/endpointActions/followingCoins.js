const db = require('../db/connectDb');

//Get user following coins
exports.getFollowingCoins = userId => {
    return new Promise((resolve, reject) => {
        const query = "SELECT * FROM following_coins WHERE user_id = $1";

        db.query(query, [userId], (err, res) => {
            if (err) {
                console.log(err)
                return reject({
                    error: err.message,
                    statusCode: err.statusCode,
                    reason: 'Database error'
                })
            }
            
            //No following coin row found, create one for user
            if (res.rows.length == 0) {
                console.log("ERROR FINDING FOLLOWING COINS FOR USER, sending empty array")
                return { coins: [] }

                /*
                return reject({
                    error: "Could not retrieve following coins for that user",
                    statusCode: 200,
                    reason: "No following coin row found for that user"
                })
                */
            }
            
            resolve({ coins: res.rows[0].saved_coins })
        });
    })
}

//Update user following coins
exports.updateFollowingCoins = (userId, coins) => {
    return new Promise((resolve, reject) => {
        const query = "INSERT INTO following_coins (user_id, saved_coins, date_saved) VALUES ($1, $2, NOW()::date) ON CONFLICT (user_id) DO UPDATE SET saved_coins = excluded.saved_coins"

        db.query(query, [userId, JSON.stringify(coins)], (err, res) => {
            if (err) {
                console.log(err)
                reject({
                    error: err.message,
                    statusCode: err.statusCode,
                    reason: 'Database error'
                })
            }

            //Count rows returned
            foundUser = res.rowCount

            if (foundUser != 1) {
                reject({
                    error: "Could not update following coins for that user",
                    statusCode: 200,
                    reason: "No user found with that userId"
                })
            }

            resolve(userId)
        });
    })
}