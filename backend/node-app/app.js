require('dotenv').config()

//Packages
const express = require("express");
const app = express();
const cors = require('cors');
const validator = require('validator');

//API ENDPOINT FILES
const newsFeed = require('./endpointActions/newsFeed');
const users = require('./endpointActions/users');
const emails = require('./endpointActions/email');
const authenticate = require('./endpointActions/authenticate');
const resetPassword = require('./endpointActions/resetPassword');
const contact = require('./endpointActions/contact');
const followingCoins = require('./endpointActions/followingCoins');


//Set to deal with CORS issues
app.use(cors())

//Allow server to accept JSON
app.use(express.json())









//////// News API //////////

//Request news feed
app.post("/api/news", (req, res) => {
    const url = req.body.apiUrl

    //Missing apiURL
    if (!req.body.apiUrl) {
        res.status(400).send({
            error: "Missing apiUrl for news feed"
        })
    }

    newsFeed.getNewsFeed(url)
        .then(response => {
            res.send({ Data: response })
        })
        .catch(error => {
            res.send({
                error: error
            })
        })
});








/////// USER SECTION ///////////////

//Create a new user
app.post("/api/user/create", (req, res) => {
    const { email, password, firstName, lastName } = req.body

    //If any input is empty reject
    if (
        validator.isEmpty(email, { ignore_whitespace: true }) ||
        validator.isEmpty(password, { ignore_whitespace: true }) ||
        validator.isEmpty(firstName, { ignore_whitespace: true }) ||
        validator.isEmpty(lastName, { ignore_whitespace: true })
    ) {
        return res.status(400).send({
            error: 'No parameters can be empty',
            reason: 'Could not create user'
        });
    }

    //Check if email is proper format
    if (!validator.isEmail(email)) {
        return res.status(400).send({
            error: 'Invalid email',
            reason: 'Email parameter is not in proper format'
        });
    }

    //Create new user
    users.createUser(email, password, firstName, lastName)
        .then(response => {
            res.status(200).send(response)
        })
        .catch(error => {
            const errorMessage = error.error
            const errorCode = error.statusCode

            res.status(errorCode).send({ error: errorMessage })
        })
})

//Create new email confirmation with magic link if user needs one
app.post('/api/user/confirmAccount/create', (req, res) => {
    const email = req.query.email;

    //If empty or bad email format
    if (validator.isEmpty(email, { ignore_whitespace: true }) || !validator.isEmail(email)) {
        return res.status(400).send({
            error: "Invalid email parameter",
            reason: "The parameter is not a valid email format"
        })
    }

    //Get user info
    users.getUserInfo(email)
        .then(response => {
            const userDetails = response.userDetails[0]
            const userId = userDetails.user_id;
            const email = userDetails.email
            const firstName = userDetails.first_name;

            //Create new confirmation token and send email
            emails.sendConfirmationEmail(userId, email, firstName)
                .then(response => {
                    res.send({ success: "Confirmation email sent" });
                })
                .catch(error => {
                    res.status(error.statusCode).send({
                        error: error.error,
                        reason: error.reason,
                    });
                });
        })
        .catch(error => {
            res.status(error.statusCode).send({
                error: error.error,
                reason: error.reason,
            })
        })
})

//Check email confirmation magic link & update user
app.post('/api/user/confirmAccount', authenticate.authenticateToken, async (req, res) => {
    //If token is valid
    if (req.success) {
        const userId = req.data.userId

        //Delete token
        emails.deleteMagicToken(userId)
            .then(() => {
                //Update user table for email confirmed
                users.confirmEmailUser(userId)
                    .then(() => {
                        res.send({ success: "User email account has been confirmed and account has been updated" })
                    })
                    .catch(error => {
                        res.status(error.statusCode).send({
                            error: error.error,
                            reason: error.reason
                        })
                    })
            })
            .catch(error => {
                res.status(500).send({
                    error: error.error,
                    reason: error.reason
                });
            })
    }
})

//Sign in a user
app.post("/api/user/login", (req, res) => {
    const email = req.body.email;
    const password = req.body.password

    //If missing either query or they are empty
    if (!email || !password || validator.isEmpty(email, { ignore_whitespace: true }) || validator.isEmpty(password, { ignore_whitespace: true })) {
        return res.status(400).send({
            error: "Could not complete sign in request",
            reason: "Missing either email or password"
        });
    }

    //Check if email format is valid
    if (!validator.isEmail(email)) {
        return res.status(400).send({
            error: "Please look at email that was used",
            reason: "Not a valid email format"
        })
    }

    //validate user send back user id
    users.loginUser(email, password)
        .then(userDetails => {
            //Create JWT tokens
            authenticate.createTokens(userDetails)
                .then(authTokens => {
                    //Append tokens to user details
                    userDetails['tokens'] = authTokens

                    res.send(userDetails)
                })
                .catch(error => {
                    const errorMessage = error.error
                    const errorCode = error.statusCode

                    res.status(errorCode).send({ error: errorMessage })
                });
        })
        .catch(error => {
            res.send(error)
        });
})

//Sign out user
app.delete("/api/user/logout", (req, res) => {
    const refreshToken = req.body.refreshToken

    //If no refresh token provided
    if (!refreshToken) {
        return res.status(400).send({
            error: "No refresh token provided",
            reason: "missing token"
        })
    }

    authenticate.deleteRefreshToken(refreshToken)
        .then(() => {
            res.status(200).send({ success: "User has been logged out" })
        })
        .catch(error => {
            const errorMessage = error.error
            const errorCode = error.statusCode

            res.status(errorCode).send({
                error: errorMessage,
                reason: error.reason,
            })
        })
})










////////// AUTH SECTION /////////////////

//Check if Access token is good
app.post("/api/token/check", authenticate.authenticateToken, (req, res) => {
    if (req.success) {
        res.send({ success: 'Access token is valid' })
    } else {
        res.status(500).send({
            error: 'An unknown error occurred',
            reason: 'unknown'
        })
    }
})

//Refresh token
app.post('/api/token/refresh', (req, res) => {
    const refreshToken = req.body.refreshToken

    //If not refresh token provided
    if (!refreshToken) {
        return res.status(401).send({
            error: 'No refresh token provided',
            reason: 'missing token',
        })
    }

    //Refresh token
    authenticate.refreshToken(refreshToken)
        .then(accessToken => {
            res.status(201).send(accessToken)
        })
        .catch(error => {
            res.status(error.statusCode).send({
                error: error.error,
                reason: error.reason
            })
        })
})








/////RESET PASSWORD SECTION ////////////////
//Create reset password request
app.post("/api/forgotPassword/create", (req, res) => {
    const email = req.query.email
    
    //Missing query or is empty
    if (!email || validator.isEmpty(email, { ignore_whitespace: true })) {
        return res.status(400).send({
            error: "Could not create a password email",
            reason: "Either missing or empty email paramater"
        });
    }

    //Check email format
    if (!validator.isEmail(email)) {
        return res.status(400).send({
            error: "Could not create a password email",
            reason: "Invalid email format"
        });
    }

    //Check for user
    users.getUserInfo(email)
        .then(response => {
            const userDetails = response.userDetails[0]
            const userId = userDetails.user_id;
            const email = userDetails.email

            //Create new forgot password reset token and send email
            resetPassword.sendResetEmail(userId, email)
                .then(response => {
                    res.send({ success: "Reset password email sent" });
                })
                .catch(error => {
                    res.status(error.statusCode).send({
                        error: error.error,
                        reason: error.reason,
                    });
                });
        })
        .catch(error => {
            res.status(500).send({
                error: error.error,
                reason: error.reason
            });
        })
})

//Check password token
app.get("/api/forgotPassword/check", authenticate.authenticateToken, (req, res) => {
    const userId = req.data.userId

    resetPassword.checkResetToken(userId)
        .then(response => {
            if (response.error) throw response
            
            res.send({ success: 'Reset password token record exists' })
        })
        .catch(error => {
            res.status(error.statusCode).send({
                error: error.error,
                reason: error.reason,
            });
        })
})

//Update the current user password with the new requested password
app.put("/api/forgotPassword/update", authenticate.authenticateToken, (req, res) => {
    const userId = req.data.userId
    const passwordVal = req.body.passwordVal

    //Check if empty or not exists
    if (!passwordVal || validator.isEmpty(passwordVal, { ignore_whitespace: true })) {
        res.status(405).send({
            error: "Could not continue update password request, missing password",
            reason: "Missing password value"
        });
    }

    //Check if correct length, 8 characters
    if (passwordVal.length < 8) {
        res.status(405).send({
            error: "Could not continue update password request, missing password",
            reason: "Invalid password length, must be 8 characters"
        });
    }

    //Update password and remove reset tokens
    resetPassword.updatePassword(userId, passwordVal)
        .then(response => {
            if (response.error) {
                throw response;
            }

            //Delete reset password token
            resetPassword.deleteResetToken(userId)
                .then(() => {
                    if (response.error) {
                        throw response;
                    }

                    //Revoke all refresh tokens
                    authenticate.deleteAllRefreshTokens(userId)
                        .then(() => {
                            return res.send({ success: "Updated user password"})
                        })
                        .catch(error => {
                            res.status(error.statusCode).send({
                                error: error.error,
                                reason: error.reason,
                            });
                        });
                })
                .catch(error => {
                    return res.status(error.statusCode).send({
                        error: error.error,
                        reason: error.reason,
                    });
                })
        })
        .catch(error => {
            return res.status(error.statusCode).send({
                error: error.error,
                reason: error.reason,
            });
        });
})





//////CONTACT SECTION /////////////////////
app.post('/api/contactUs', (req, res) => {
    const name = req.body.name;
    const email = req.body.email;
    const bodymessage = req.body.bodymessage

    //If email OR message empty
    if (!req.body.email || !req.body.bodymessage) {
        return res.status(400).send({
            error: "Could not send contact message",
            reason: "Missing a parameter"
        })
    }

    //Check email format
    if (!validator.isEmail(email)) {
        return res.status(400).send({
            error: "Could not send contact message",
            reason: "Email is not proper format"
        })
    }

    //Send the email to our team
    contact.sendContactEmail(name, email, bodymessage)
        .then(() => {
            res.send({ success: "Contact message has been sent to the BlockFade team"})
        })
        .catch(error => {
            console.log(error)
        
            return res.status(error.statusCode).send({
                error: error.error,
                reason: error.reason,
            });
        })
})



///FOLLOWING COINS SECTION/////////////////////

///Get users following coins
app.get('/api/followingCoins/coins', (req, res) => {
    const {userId} = req.query;

    //No userId or following Coins provided
    if (!userId || userId === "") {
        return res.status(400).send({
            error: "Could not complete save request for following coins",
            reason: "Missing userId parameter"
        });
    }

    //Get the following coins for the user from the database
    followingCoins.getFollowingCoins(userId)
        .then(response => {
            res.send(response);
        })
        .catch(error => {
            console.log(error);

            return res.status(error.statusCode).send({
                error: error.error,
                reason: error.reason,
            })
        });
})

///Update users following coins
app.put('/api/followingCoins/update', (req, res) => {
    const {userId, coins} = req.body;

    //No userId or following Coins provided
    if (!userId || !coins || userId === "") {
        return res.status(400).send({
            error: "Could not complete save request for following coins",
            reason: "Missing either userId or coins parameter"
        });
    }

    //Update the database to reflect new following coins
    followingCoins.updateFollowingCoins(userId, coins)
        .then(() => {
            res.send({ success: "Updated user following coins" });
        })
        .catch(error => {
            console.log(error);

            return res.status(error.statusCode).send({
                error: error.error,
                reason: error.reason,
            })
        });
})


//Launch the server
app.listen(3001, () => console.log('Server started on port 3001'));
