const { Pool } = require('pg')
const fs = require('fs');
const path = require('path')

const pool = new Pool({
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    database: process.env.DB_DATABASE,
    password: process.env.DB_PASSWORD,
    port: process.env.DB_PORT,
    ssl: {
        rejectUnauthorized: false,
        ca: fs.readFileSync(path.resolve(__dirname, 'rds-ca-2019-root.pem')).toString(),
    },
});

exports.query = (text, params, callback) => {
    return pool.query(text, params, callback)
}
