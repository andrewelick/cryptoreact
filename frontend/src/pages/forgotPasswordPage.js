import React from 'react';
import { ForgotPasswordSend } from '../components/forgotPassword/forgotPasswordSend';
import { ForgotPasswordReset } from '../components/forgotPassword/forgotPasswordReset';
import { Facebook } from 'react-spinners-css';

import './css/forgotPasswordPage.css';

const queryString = require('query-string');


export class ForgotPasswordPage extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            loadingItems: true,
            resetTokenPresent: false,
        }
    }

    componentDidMount() {
        //Hide loading icon
        this.setState({ loadingItems: false })

        //Load in email from query params
        const params = queryString.parse(window.location.search)

        //If reset token in query string
        if (params.token) {
            this.setState({
                resetTokenPresent: true,
                resetToken: params.token,
            })
        }
    }

    render() {

        //If loading components
        if (this.state.loadingItems) {
            return (
                <div style={{ textAlign:"center" }} className="forgotPasswordContainer">
                    <Facebook color={"#1652f0"} size={50} />
                </div>
            )
        }

        return (
            <div className="forgotPasswordContainer">
                {this.state.resetTokenPresent ? <ForgotPasswordReset resetToken={this.state.resetToken} /> : <ForgotPasswordSend />}
            </div>
        )
    }
}