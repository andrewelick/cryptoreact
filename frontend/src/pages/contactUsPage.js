import React from 'react';
import { ContactUsCard } from '../components/contact/contactUsCard';
import './css/contactUsPage.css';

export class ContactUsPage extends React.Component {
    render() {
        return (
            <div className="contactUsContainer">
                <img className="contactUsImg" src="/message.png" alt="message" />
                <div className="contactUsHeader">Shoot us a message!</div>
                <div>If you have any ideas, suggestions, or issues please let us know and we will get back to you as soon as possible</div>
                <br />
                <ContactUsCard />
            </div>
        )
    }
}