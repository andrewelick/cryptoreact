import React from 'react';
import { CoinCard } from '../components/coin/coinCard';
import { CoinTools } from '../components/coin/coinTools';
import { Facebook } from 'react-spinners-css';

import './css/coinPage.css';

export class CoinPage extends React.Component {
    abortController = new AbortController()

    constructor(props) {
        super(props)

        this.state = {
            coinInfo: null,
            dateRange: '24h',
        }
    }

    //Fetch coin information
    fetchCoinInfo = coinId => {
        const url = "https://api.coingecko.com/api/v3/coins/" + coinId + "?localization=false&tickers=false&market_data=true&community_data=true&developer_data=true&sparkline=true";

        fetch(url, { signal: this.abortController.signal })
            .then(response => response.json())
            .then(response => {
                return this.setState({ coinInfo: response })
            })
            .catch(error => {
                console.log(error)
            })
    }

    componentDidMount() {
        //Grab coin ID from URL
        const { coinId } = this.props.match.params
        this.fetchCoinInfo(coinId);
    }

    componentWillUnmount() {
        this.abortController.abort()
    }

    render() {
        //Loading coin data
        if (!this.state.coinInfo) {
            return <div className="coinPageContainer"><Facebook color="#FFFFFF" /></div>
        }

        //Chart variables
        const coinId = this.state.coinInfo.id;
        const coinSymbol = this.state.coinInfo.symbol;
        
        return (
            <div className="coinPageContainer">
                <div className="coinpageCoinCardContainer">
                    <CoinCard 
                        coinId = {coinId}
                        data = {this.state}
                        visibility = {{
                            header: true,
                            price: true,
                            daily: true,
                            dateRangePicker: true,
                            chart: true,
                            marketStats: true,
                            info: true,
                            news: true,
                        }}
                    />
                </div>

                <div className="coinToolsContainer">
                    <CoinTools coinId={coinId} coinSymbol={coinSymbol} />
                </div>
            </div>
        )
    }
}