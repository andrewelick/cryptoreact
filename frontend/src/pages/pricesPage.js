import React from 'react';
import { PricesList } from '../components/prices/PricesList';
import { DateRangePicker } from '../components/dateRange/DateRangePicker';
import '../components/prices/prices.css'

export class PricesPage extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            dateRange: '24h',
            searchResult: null,
        }
    }

    //Change the time for percentage
    changeTime = timeVal => {
        return this.setState({ dateRange: timeVal })
    }

    //Filter results of coins
    filterCoins = event => {
        const searchResult = event.target.value;
        return this.setState({ searchResult: searchResult })
    }

    render() {
        return (
            <div>
                <div className="pricesPageHeaderContainer">
                    <input type="text" onChange={this.filterCoins} className="pricesPageHeaderSearch" placeholder="Search for cryptocurrency" />
                    
                    <div className="pricesPageDatePicker">
                        <DateRangePicker changeTime={this.changeTime} />
                    </div>
                    
                    <div className="pricesPageTitle">Most popular cryptocurrencies</div>
                </div>

                <PricesList selectedTime={this.state.dateRange} searchResult={this.state.searchResult} />
            </div>
        )
    }
}