import React from 'react';
import { Redirect } from 'react-router-dom';
import { LoginCard } from '../components/login/loginCard';

export class LoginPage extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            authStatus: this.props.authStatus,
        }
    }

    componentDidUpdate(nextprops) {
        //Update auth status on change
        const { authStatus } = this.props

        if (nextprops.authStatus !== authStatus) {
            if (authStatus) {
                this.setState({ authStatus: authStatus })
            }
        }
    }

    render() {

        if (this.state.authStatus.hasAuth) {
            return <Redirect to="/" />
        }

        return (
            <div>
                <LoginCard handleAuth={this.props.handleAuth} />
            </div>
        )
    }
}