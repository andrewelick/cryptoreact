import React from 'react';
import { debounce } from 'lodash';
import { LandingCard } from '../components/landingCard/LandingCard';
import { CoinCard } from '../components/coin/coinCard';
import { FollowingCoins } from '../components/followingCoins/followingCoins';
import './css/homePage.css';

export class HomePage extends React.Component {
    abortController = new AbortController()

    constructor(props) {
        super(props)

        this.state = {
            authStatus: this.props.authStatus,
            screenView: null,
            chosenCoins: [],
        }

        //Hand picked options of coins to show on front page
        this.cryptoOptions = [{ id: 'bitcoin' }, { id: 'litecoin' }, { id: 'ethereum' }, { id: 'ripple' }, { id: 'chainlink'}, { id: 'bitcoin-cash' }]
        
        //Style for coinContainer
        this.coinCardStyle = {
            container: {
                desktop: {
                    width: "48%",
                    margin: "15px 0 0 0",
                },
                mobile: {}
            }
        }
    }

    //Load a coin card with it's data
    loadCoinCard = coin => {
        return (
            //Card element that displays a coin with market info
            <CoinCard
                key = {'coinCard-'+ coin} 
                coinId = {coin}
                visibility = {{
                    header: true,
                    price: true,
                    dateRangePicker: true,
                    chart: true,
                }}
                style = {{
                    container: this.chooseStyle("coinCardStyle")
                }}
            />
        )
    }

    //Get window size & update state
    resizeWindow = () => {
        const newWidth = window.innerWidth;
        const newHeight = window.innerHeight;
        const oldWidth = this.state.widthWindow;
        const oldHeight = this.state.heightWindow;
        let screenView;

        //To save start updates
        if (newWidth !== oldWidth || newHeight !== oldHeight) {
            if (newWidth > 650) {
                screenView = "desktop";
            } else {
                screenView = "mobile";
            }

            //Screen view has changed from desktop/mobile
            if (this.state.screenView !== screenView) {
                this.setState({ screenView })
            }
        }
    }

    //Decide if mobile style for coin card
    chooseStyle = componentStyle => {
        const screenView = this.state.screenView

        if (screenView === "mobile") {
            return this[componentStyle].container.mobile;
        }

        return this[componentStyle].container.desktop;
    }

    //Load following coins
    loadFollowingCoins = userId => {
        //Clear chosen coins
        this.setState({
            chosenCoins: [],
        });

        const url = "https://blockfade.com/api/followingCoins/coins?userId=" + userId;
        
        fetch(url)
            .then(response => response.json())
            .then(response => {
                this.setState({
                    chosenCoins: response.coins
                })
            })
            .catch(error => {
                console.log(error)
            })
    }

    componentDidUpdate(prevprops) {
        //Update auth status on change
        const { authStatus } = this.props

        if (prevprops.authStatus !== authStatus) {
            if (authStatus) {
                this.setState({ authStatus: authStatus }, () => {
                    //If user logged in
                    if (authStatus.hasAuth) {
                        //Load their following coins for user
                        this.loadFollowingCoins(this.props.authStatus.userDetails.userId);
                    } else {
                        //Use predefined list for coincard for user not logged in
                        this.setState({ chosenCoins: this.cryptoOptions })
                    }
                })
            }
        }
    }

    componentDidMount() {
        //Tell react to look for window resizing
        window.addEventListener("resize", debounce(this.resizeWindow.bind(this), 100));
        //Get window size
        this.resizeWindow();

        //If user logged in
        if (this.state.authStatus.hasAuth) {
            //Load the following coins after user auth has happened
            this.loadFollowingCoins(this.props.authStatus.userDetails.userId);
        } else {
            //Use predefined list for coincards of nonlogged in user
            this.setState({ chosenCoins: this.cryptoOptions });
        }
    }

    componentWillUnmount() {
        this.abortController.abort()
    }

    render() {
        const hasAuth = this.state.authStatus.hasAuth
        let userId;

        if (hasAuth) {
            userId = this.state.authStatus.userDetails.userId;
        }
        
        return (
            <div>
                {/*Show landing card based on if user is logged in*/}
                {hasAuth ? null : <LandingCard hasAuth={hasAuth} />}

                <div className="homepageCoinCardContainer">
                    {/*Show users following coins if logegd in*/}
                    {
                        hasAuth ?
                        <FollowingCoins refreshChosenCoins={this.loadFollowingCoins} userId={userId} />
                        : null
                    }

                    {/*Show coinCards when not editing following coins */}
                    {
                        this.state.chosenCoins.map(coin => {
                            return this.loadCoinCard(coin.id);
                        })
                    }
                </div>
            </div>

        )
    }
}