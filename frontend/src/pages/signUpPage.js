import React from 'react';
import { SignUpCard } from '../components/signUp/signUp';
import { Redirect } from 'react-router-dom';

export class SignUpPage extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            authStatus: this.props.authStatus,
        }
    }

    componentDidUpdate(nextprops) {
        //Update auth status on change
        const { authStatus } = this.props

        if (nextprops.authStatus !== authStatus) {
            if (authStatus) {
                this.setState({ authStatus: authStatus })
            }
        }
    }


    render() {

        if (this.state.authStatus.hasAuth) {
            return <Redirect to="/" />
        }

        return (
            <div>
                <SignUpCard handleAuth={this.props.handleAuth} />
            </div>
        )
    }
}