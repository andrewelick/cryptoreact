import React from 'react';
import { Redirect } from 'react-router-dom';
import { ConfirmEmailCard } from '../components/confirmEmail/confirmEmailCard';

const queryString = require('query-string');

export class ConfirmEmailPage extends React.Component {
    constructor(props) {
        super(props)

        //For redirecting page if not token param
        this.tokenPresent = true

        this.state = {
            emailConfirmed: null,
            errorMessage: null,
            showNewTokenButton: false,
            showContactButton: false,
        }
    }

    //Check token if valid and update user if so
    checkConfirmToken = token => {
        return new Promise((resolve, reject) => {
            if (!token) {
                reject({
                    error: 'Could not run token validation',
                    reason: 'Missing token'
                })
            }

            const requestUrl = "https://blockfade.com/api/user/confirmAccount";
            const bearerHeader = 'Basic ' + token
            const headers = {
                'Content-Type': 'application/json',
                'Authorization': bearerHeader
            }

            //Make API to validate token and user confirmation request
            fetch(requestUrl, {
                method: 'post',
                headers: headers
            })
                .then(response => response.json())
                .then(response => {
                    if (response.error) {
                        reject(response)
                    } else {
                        resolve(response)
                    }
                }) 
                .catch(error => {
                    reject(error)
                });
        });
    }

    //Check new magic link email input
    checkMagicLinkEmail = emailVal => {
        return new Promise((resolve, reject) => {
            if (emailVal === "" || emailVal === null) {
                return reject({
                    error: 'Email cannot be empty'
                })
            } else {
                const requestUrl = "https://blockfade.com/api/user/confirmAccount/create?email="+emailVal;
                //Send a new email confirmation
                fetch(requestUrl, {
                    method: 'post',
                })
                    .then(response => response.json())
                    .then(response => {
                        return resolve(response)
                    })
                    .catch(error => {
                        return reject(error)
                    })
            }
        });
    }

    componentDidMount() {
        //Load in email from query params
        const params = queryString.parse(window.location.search)

        //We check if token param to see if we redirect or not
        if (!params.token) {
            this.tokenPresent = false;
        } else {
            const token = params.token

            //Validate token & activate account & display errors and state changes
            this.checkConfirmToken(token)
                .then(() => {
                    //Get localstorage
                    let userData = JSON.parse(localStorage.getItem('userDetails'));

                    //Update user data
                    if (userData) {
                        //Clear the user data
                        localStorage.clear()

                        //Update email confirmed and save storage
                        userData.emailConfirmed = true
                        localStorage.setItem('userDetails', JSON.stringify(userData))
                    }

                    this.setState({ emailConfirmed: true })
                })
                .catch(error => {
                    const reason = error.reason
                    let errorMessage = null
                    let showNewTokenButton = false
                    let showContactButton = false

                    //Set the error message based on error sent back
                    switch (reason) {
                        case 'expired':
                            showNewTokenButton = true
                            errorMessage = "This link is no longer valid, if you need a new one please enter your email and we'll send it again";
                            break
                        case 'No confirm token':
                            showContactButton = true
                            errorMessage = "We believe your account has already been activated, please try logging in if you have not done so. If this is an error on our part please contact us"
                            break;
                        default:
                            console.log(error)
                            errorMessage = "Looks like the connection is down or something on our end broke, we're sorry"
                    }

                    //Updates state for ConfirmEmailCard Props
                    this.setState({
                        emailConfirmed: false,
                        errorMessage,
                        showContactButton,
                        showNewTokenButton
                    });
                });
        }
    }

    render(){
        //If no token param then redirect
        if (!this.tokenPresent) {
            return <Redirect to="/" />
        }

        return (
            <div className="confirmEmailCardContainer">
                <ConfirmEmailCard emailConfirmed={this.state.emailConfirmed} errorMessage={this.state.errorMessage} showNewTokenButton={this.state.showNewTokenButton} showContactButton={this.state.showContactButton} checkMagicLinkEmail={this.checkMagicLinkEmail} />
            </div>
        )
    }
}