import React from 'react';
import { Profile } from '../components/profile/Profile'

export class ProfilePage extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            authStatus: this.props.authStatus,
        }
    }

    componentDidUpdate(nextprops) {
        //Update auth status on change
        const { authStatus } = this.props

        if (nextprops.authStatus !== authStatus) {
            if (authStatus) {
                this.setState({ authStatus: authStatus })
            }
        }
    }

    render() {
        const hasAuth = this.state.authStatus.hasAuth
        const userDetails = this.state.authStatus.userDetails

        return (
            <Profile hasAuth={hasAuth} userDetails={userDetails} />
        )
    }
}