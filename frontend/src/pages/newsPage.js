import React from 'react';
import { News } from '../components/news/news';

export class NewsPage extends React.Component {

    render() {
        return (
            <div style={{ padding: 20, margin: 'auto', maxWidth: '900px' }}>
                <h2>Latest stories</h2>
                <News />
            </div>
        )
    }
}