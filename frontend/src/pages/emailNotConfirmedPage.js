import React from 'react';
import { Facebook } from 'react-spinners-css';
import './css/emailNotConfirmedPage.css';

export class EmailNotConfirmedPage extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            sendingEmail: false,
            emailSent: false,
            error: false,
        }
    }

    //Send another confirmation email to user
    sendAnotherEmail = () => {
        const emailVal = this.props.authStatus.userDetails.email
        const requestUrl = "https://blockfade.com/api/user/confirmAccount/create?email="+emailVal;
        
        //Send request to API
        fetch(requestUrl, {
            method: 'post',
        })
            .then(response => response.json())
            .then(() => {
                //To show error message & hide spinner
                return this.setState({
                    sendingEmail: false,
                    emailSent: true,
                });
            })
            .catch(error => {
                console.log(error)

                //To show error message & hide spinner
                return this.setState({
                    sendingEmail: false,
                    emailSent: true,
                    error: true,
                });
            });
    }

    render() {
        const error = this.state.error
        const emailSent = this.state.emailSent
        
        const sentMessage = <div className={error ? "errorColor" : "successColor"}>{error ? "There was an error sending a new email" : "Success! There should be a new one in your inbox soon"}</div>
        
        return (
            <div className="emailNotConfirmedPageContainer">
                <img className="emailNotConfirmedImg" src="/anonymous.png" alt="anonymous" />
                <div className="emailNotConfirmedTitle">Oops, you haven't confirmed you email account yet!</div>
                <div className="emailNotConfirmedBody">
                    Please check your inbox and spam folder for a message from and us and follow the included instructions.

                    <div className="emailNotConfirmedBodySendBody">
                        <div className="emailNotConfirmedSentMessage">
                            {emailSent ? sentMessage : null }
                        </div>
                        Don't see it?
                        <button onClick={this.sendAnotherEmail} disabled={this.state.sendingEmail} className="emailNotConfirmedSendButton">
                            {this.state.sendingEmail ? <Facebook size={24} color="#FFFFFF" />: "Send me another"}
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}