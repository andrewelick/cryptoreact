import React from 'react';
import './tools.css';
import Numeral from 'numeral';

export class PriceConvertor extends React.Component {
    abortController = new AbortController()

    constructor(props) {
        super(props)

        this.state = {
            inputAmount: 1,
            coinCurrentPrice: null,
            currencies: null
        }
    }

    //Get the current echange rates
    fetchExchangeRates = coinId => {
        const url = "https://api.coingecko.com/api/v3/coins/" + coinId + "?localization=false&tickers=false&market_data=true&community_data=false&developer_data=false&sparkline=false";

        fetch(url, { signal: this.abortController.signal })
            .then(response => response.json())
            .then(list => {
                const currencyList = list.market_data.current_price
                this.setState({ currencies: currencyList })
            })
            .catch(error => console.log(error))
    }

    convertPrice = () => {
        const { inputAmount, coinCurrentPrice } = this.state
        return inputAmount * coinCurrentPrice
    }

    //Update final rate after user input
    handleChange = event => {
        const inputVal = event.target.value;
        this.setState({ inputAmount: inputVal })
    }

    //Change the currency
    handleCurrencyChange = event => {
        const newCurrency = event.target.value
        this.setState({ coinCurrentPrice: newCurrency })
    }

    componentDidMount() {
        this.fetchExchangeRates(this.props.coinId)
        this.convertPrice(this.state.inputAmount, this.state.coinCurrentPrice)
    }

    componentWillUnmount() {
        this.abortController.abort()
    }

    render() {
        const convertPrice = Numeral(this.convertPrice(this.state.inputAmount, this.state.coinCurrentPrice)).format('0,0.00')
        let currencySelect = null

        //Populate dropdown with currencies
        if (this.state.currencies) {
            currencySelect = Object.entries(this.state.currencies).map(currency => {
                const name = currency[0].toUpperCase()
                const price = currency[1]

                if (name === "USD") return <option key={name} selected="selected" value={price}>{name}</option>

                return <option key={name} value={price}>{name}</option>
            })
        }

        //Select USD as default currency
        if (this.state.currencies && this.state.coinCurrentPrice === null) {
            const usd = this.state.currencies.usd
            this.setState({ coinCurrentPrice: usd })
        }

        return (
            <div className="priceConvertor">
                <div>
                    <input onChange={this.handleChange} type="text" className="priceConvertorInput" value={this.state.inputAmount} />
                    {this.props.coinSymbol.toUpperCase()}
                </div>
                <div className="priceConvertorPriceResult">
                    {convertPrice}

                    <select onChange={this.handleCurrencyChange} className="priceConvertorPriceSelect">
                        {currencySelect}
                    </select>
                </div>
            </div>
        )
    }
}