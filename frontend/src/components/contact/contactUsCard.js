import React from 'react';
import { Facebook } from 'react-spinners-css';
import './contactus.css';

export class ContactUsCard extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            name: '',
            email: '',
            bodymessage: '',
            sendingRequest: false,
            messageSent: false,
            error: false,
            message: '',
        }
    }

    handleNameChange = event => {
        const nameVal = event.target.value;
        this.setState({ name: nameVal })
    }

    handleEmailChange = event => {
        const emailVal = event.target.value
        this.setState({ email: emailVal })
    }

    handleMessageChange = event => {
        const messageVal = event.target.value
        this.setState({ bodymessage: messageVal })
    }

    //Validate inputs and send email to our inbox
    sendContactMessage = () => {
        //To show loading icon and disable button
        this.setState({
            sendingRequest: true,
            error: false,
            message: '',
        })

        //If email or message left blank
        if (this.state.email === "" || this.state.bodymessage === "") {
            return this.setState({
                error: true,
                message: 'Please fill out the required fields',
                sendingRequest: false,
            })
        }

        //Request information
        const { name, email, bodymessage } = this.state
        const requestUrl = "https://blockfade.com/api/contactUs";
        const data = { name, email, bodymessage }
        
        fetch(requestUrl, {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then(response => response.json())
            .then(response => {
                if (response.error) throw response

                return this.setState({
                    error: false,
                    sendingRequest: false,
                    messageSent: true,
                })
            })
            .catch(error => {
                this.setState({
                    error: true,
                    message: 'There was a server error while sending your message. Please try again later'
                })
            })
    }

    render() {

        //If contact message was sent successfully
        if (this.state.messageSent) {
            return (
                <div>
                    <div className="contactUsSuccess">You message has been sent to our team!</div>
                </div>
            )
        }

        return (
            <div>
                <div className="contactUsError">{this.state.message}</div>
                <label className="contactUsInputLabel">Name</label>
                <input onChange={this.handleNameChange} type="text" className="contactUsInput" placeholder="Full Name" />
                
                <label className="contactUsInputLabel">
                    Email <span className="contactUsRequired">*</span>
                </label>
                <input onChange={this.handleEmailChange} type="text" className="contactUsInput" placeholder="Email" />
                
                <label className="contactUsInputLabel">
                    Message <span className="contactUsRequired">*</span>
                </label>
                <textarea onChange={this.handleMessageChange} type="text" className="contactUsInput" placeholder="Message" style={{ resize:'none' }}></textarea>
                
                <button onClick={this.sendContactMessage} disabled={this.state.sendingRequest} className="contactUsButton">{this.state.sendingRequest ? <Facebook color="#FFFFFF" size={22} /> : "Send Message" }</button>
            </div>
        )
    }
}