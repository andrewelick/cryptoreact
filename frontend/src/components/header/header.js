import React from 'react';
import { HeaderDropDown } from './headerDropDown';
import { HeaderMobileMenu } from './headerMobileMenu'
import { Link } from 'react-router-dom';
import './header.css';

export class Header extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            authStatus: this.props.authStatus,
            dropDownShow: false,
            mobileMenuShow: false,
        }
    }

    //Sign out user and update state
    handleSignOut = () => {
        const userSignedOut = this.props.signOutUser()

        if (userSignedOut.error) {
            const error = userSignedOut.error
            console.log(error)
        }

        //Reset auth state
        return this.setState({
            authStatus: { hasAuth: false },
        })
    }

    //Show user menu
    showMenu = () => {
        this.setState({ dropDownShow: true }, () => {
            document.addEventListener('click', this.closeMenu);
        })
    }

    //Hide user menu
    closeMenu = event => {
        const elementName = event.target.className.split(" ")[0];
        const dropDownClasses = ['headerDropDownMenu', 'headerDropDownRow', 'headerDropDownItem']

        if (!dropDownClasses.includes(elementName)) {
            this.setState({ dropDownShow: false }, () => {
                document.removeEventListener('click', this.closeMenu);
            });
        }
    }

    //Show mobile menu
    showMobileMenu = () => {
        //Lock scroll
        document.body.style.overflow = "hidden"
        this.setState({ mobileMenuShow: true })
    }

    //Hide mobile menu
    hideMobileMenu = () => {
        //Unlock scroll
        document.body.style.overflow = "scroll"
        this.setState({ mobileMenuShow: false })
    }

    componentDidUpdate(nextprops) {
        //Update auth status on change
        const { authStatus } = this.props

        if (nextprops.authStatus !== authStatus) {
            if (authStatus) {
                this.setState({ authStatus: authStatus })
            }
        }
    }

    render() {
        //Auth state of user
        const authStatus = this.state.authStatus.hasAuth

        //If user is logged in
        if (authStatus) {

            return (
                <div className="headerContainer">

                    <Link to='/'>
                        <span className="headerWebsiteName">
                            Block<span className="headerWebsiteNameThin">Fade</span>
                        </span>
                    </Link>

                    <div className='headerDesktopBar'>
                        <div className="headerItem">
                            <img onClick={this.showMenu} className="headerPicture" src="/user.png" alt="userpic" />
                            {this.state.dropDownShow ? <HeaderDropDown userDetails={this.state.authStatus.userDetails} signOutUser={this.handleSignOut} /> : null}
                        </div>

                        <Link to='/news'>
                            <span className="headerItem">News</span>
                        </Link>

                        <Link to='/prices'>
                            <span className="headerItem">Prices</span>
                        </Link>
                    </div>

                    <img className="mobileMenuIcon" onClick={this.showMobileMenu} src="/hamburger-icon.png" alt="mobile menu icon" />

                    {this.state.mobileMenuShow ? <HeaderMobileMenu userDetails={this.state.authStatus.userDetails} signOutUser={this.handleSignOut} hideMobileMenu={this.hideMobileMenu} /> : null}

                </div>
            )
        } else {
            //Not logged in
            return (
                <div className="headerContainer">

                    <Link to='/'>
                        <span className="headerWebsiteName">
                            Block<span className="headerWebsiteNameThin">Fade</span>
                        </span>
                    </Link>

                    <div className='headerDesktopBar'>
                        <Link to='/signup?email'>
                            <span className="headerItem">
                                <button className="headerJoinButton">Join Today</button>
                            </span>
                        </Link>

                        <Link to='/login'>
                            <span className="headerItem">Sign in</span>
                        </Link>

                        <Link to='/news'>
                            <span className="headerItem">News</span>
                        </Link>

                        <Link to='/prices'>
                            <span className="headerItem">Prices</span>
                        </Link>
                    </div>

                    <img className="mobileMenuIcon" onClick={this.showMobileMenu} src="/hamburger-icon.png" alt="mobile menu icon" />

                    {this.state.mobileMenuShow ? <HeaderMobileMenu hideMobileMenu={this.hideMobileMenu} /> : null}
                </div>
            )
        }
    }
}