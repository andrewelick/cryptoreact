import React from 'react';
import { Link } from 'react-router-dom';
import './header.css';

export class HeaderMobileMenu extends React.Component {
    constructor(props) {
        super(props)
    }

    //Sign out user 
    signOutClick = () => {
        this.props.signOutUser();
        this.props.hideMobileMenu();
    }

    //Handle click of menu close
    handleClick = () => {
        this.props.hideMobileMenu()
    }

    render() {

        //If user is logged
        if (this.props.userDetails) {
            return (
                <div className="headerMobileMenu">
                    <Link to='/'>
                        <span onClick={this.handleClick} className="headerWebsiteName MobileMenuName">
                            Block<span className="headerWebsiteNameThin MobileMenuNameThin">Fade</span>
                        </span>
                    </Link>

                    <img className="mobileMenuIcon" onClick={this.handleClick} src="/exit-icon.png" />

                    <div className="headerItemMobile">
                        <Link to='/prices' onClick={this.handleClick}>Prices</Link>
                    </div>

                    <div className="headerItemMobile">
                        <Link to='/news' onClick={this.handleClick}>News</Link>
                    </div>

                    <div className="headerItemMobile">
                        <Link to='/profile' onClick={this.handleClick}>Profile</Link>
                    </div>

                    <div className="headerItemMobile">
                        <Link to='/signup?email'>
                            <button onClick={this.signOutClick} className="headerItemLogoutButton">Sign out</button>
                        </Link>
                    </div>

                </div>
            )
        }

        return (
            <div className="headerMobileMenu">
                <Link to='/'>
                    <span onClick={this.handleClick} className="headerWebsiteName MobileMenuName">
                        Block<span className="headerWebsiteNameThin MobileMenuNameThin">Fade</span>
                    </span>
                </Link>

                <img className="mobileMenuIcon" onClick={this.handleClick} src="/exit-icon.png" alt="mobile menu exit icon" />

                <div className="headerItemMobile">
                    <Link to='/prices' onClick={this.handleClick}>Prices</Link>
                </div>

                <div className="headerItemMobile">
                    <Link to='/news' onClick={this.handleClick}>News</Link>
                </div>

                <div className="headerItemMobile">
                    <Link to='/login'>
                        <button onClick={this.handleClick} className="headerJoinButton LoginButton">Sign In</button>
                    </Link>
                </div>

                <div className="headerItemMobile">
                    <Link to='/signup?email'>
                        <button onClick={this.handleClick} className="headerJoinButton">Join Today</button>
                    </Link>
                </div>
            </div>
        )
    }
}