import React from 'react';
import { Link } from 'react-router-dom';


export class HeaderDropDown extends React.Component {

    //Sign out user 
    handleClick = async () => {
        this.props.signOutUser();
    }

    render() {
        const userFirstName = this.props.userDetails.firstName
        const userLastName = this.props.userDetails.lastName
        const userEmail = this.props.userDetails.email

        return (
            <div className="headerDropDownMenu">
                <div className="headerDropDownRow">
                    <div className="headerDropDownItem headerItemName">{userFirstName} {userLastName}</div>
                    <div className="headerDropDownItem headerItemEmail">{userEmail}</div>
                </div>

                <div className="headerDropDownRow">
                    <Link to="/profile">
                        <div className="headerDropDownItem">Settings</div>
                    </Link>
                </div>

                <div className="headerDropDownRow">
                    <div onClick={this.props.signOutUser} className="headerDropDownItem headerItemLogout">Sign Out</div>
                </div>
            </div>
        )
    }
}