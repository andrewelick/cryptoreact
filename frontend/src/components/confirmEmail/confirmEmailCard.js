import React from 'react';
import { Facebook } from 'react-spinners-css';
import { Link } from 'react-router-dom';
import './confirmEmailCard.css';

export class ConfirmEmailCard extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            newEmailInputVal: '',
            newTokenSuccess: false,
            newTokenErrorMessage: null,
        }
    }

    //Send a new verification email
    handleEmailClick = async () => {
        this.props.checkMagicLinkEmail(this.state.newEmailInputVal)
            .then(response => {
                if (response.error) {
                    let errorMessage
                    
                    if (response.reason === 'No user found') {
                        errorMessage = "We could not find any accounts with that email address";
                    } else {
                        console.log(response)
                        errorMessage = "An unkown error has occured"
                    }

                    //To show the error message
                    return this.setState({ newTokenErrorMessage: errorMessage })
                }

                //To show that the magic link worked
                this.setState({ newTokenSuccess: true })
            })
            .catch(error => {
                //To show the error message
                this.setState({ newTokenErrorMessage: error.error })
            })
    }

    //Tracks the value of the new email
    handleEmailInputChange = event => {
        const newVal = event.target.value;
        this.setState({ newEmailInputVal: newVal })
    }

    render() {
        const {emailConfirmed, errorMessage, showNewTokenButton, showNewContactButton } = this.props

        const newMagicLinkElements = (
            <div className="confirmEmailCardNewEmailContainer">
                <input onChange={this.handleEmailInputChange} className={this.state.newTokenSuccess ? "confirmEmailCardHidden" : "confirmEmailCardNewEmailInput"} type="text" placeholder="Email address" />
                <button onClick={this.handleEmailClick} className={this.state.newTokenSuccess ? "confirmEmailCardHidden" : "confirmEmailCardNewEmailButton"}>Send</button>
                <div className={this.state.newTokenSuccess ? "confirmEmailCardHidden" : "confirmEmailCardNewEmailError"}>{this.state.newTokenErrorMessage}</div>
                <div className="confirmEmailCardNewEmailSuccess">{this.state.newTokenSuccess ? "Success! We have sent another confirmation link to your email account" : null}</div>
                <div className="confirmEmailCardNewEmailExplainText">This link will expire after 24 hours after it's sent, so hurry!</div>
            </div>
        )

        //If email verification has not completed
        if (emailConfirmed === null) {
            return <Facebook className="loadingSpinner" color="#FFFFFF" />
        }

        return (
            <div className="confirmEmailCard">
                <div className="confirmEmailCardTitle">
                    {emailConfirmed ? <div>Congrats your verified! <img className="confirmEmailCardImg" src="/balloons.png" /></div> : <div>Something went wrong <img style={{ position:'relative', top: '-9px' }} className="confirmEmailCardImg" src="/error.png" /></div>}
                </div>

                <div className="confirmEmailCardMessage">
                    {emailConfirmed ? <div>We appreciate you joining our community and if there is anything we can do to make this place better please let us know! <Link to="/login"><button className="confirmEmailCardGoButton">Lets go!</button></Link></div> : errorMessage}
                    {showNewTokenButton ? newMagicLinkElements : null }
                </div>
            </div>
        )
    }
}