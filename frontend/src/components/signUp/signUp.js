import React from 'react';
import { Facebook } from 'react-spinners-css';
import { isEmpty, isEmail } from 'validator';
import './signUp.css';
const queryString = require('query-string');

export class SignUpCard extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            firstName: '',
            lastName: '',
            email: '',
            password: '',
            confirmPassword: '',
            mainError: false,
            mainErrorMessage: '',
            errors: {},
            sendingRequest: false,
            buttonDisabled: false,
        }
    }

    //Send creds to backend and save user
    createNewUser = () => {
        const { firstName, lastName, email, password } = this.state;
        const url = "https://blockfade.com/api/user/create"

        //Show loader to user while waiting on request
        this.setState({ 
            sendingRequest: true,
            buttonDisabled: true,
        })

        fetch(url, {
            method: 'post',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                email,
                password,
                firstName,
                lastName
            })
        })
            .then(response => response.json())
            .then(response => {
                //If email is already being used
                if (response.error) {
                    return this.setState({
                        mainError: true,
                        mainErrorMessage: response.error,
                        sendingRequest: false,
                        buttonDisabled: false,
                    })
                }

                if (response.success) {
                    //Sign in user after
                    this.signInUser(email, password)
                        .then(() => { return this.props.handleAuth() })
                        .catch(error => {
                            console.log(error)
                        })
                }
            })
            .catch(error => {
                return this.setState({
                    mainError: true,
                    mainErrorMessage: 'There was an error connecting to BlockFade, please try again',
                    sendingRequest: false,
                    buttonDisabled: false,
                })
            })
    }

    //Sign user in
    signInUser = (email, password) => {
        return new Promise((resolve, reject) => {
            //Url of API endpoint
            const url = 'https://blockfade.com/api/user/login';

            //Send request to API
            fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    email,
                    password
                })
            })
                .then(response => response.json())
                .then(response => {
                    //Save user details to local storage
                    localStorage.setItem('userDetails', JSON.stringify(response));
                    resolve(response)
                })
                .catch(error => {
                    reject(error)
                });
        });
    }

    //Validate the forms
    validateForms = () => {
        const { firstName, lastName, email, password, confirmPassword } = this.state;
        const credsList = [firstName, lastName, email, password, confirmPassword]
        const errorsList = [this.state.firstNameError, this.state.lastNameError, this.state.emailError, this.state.passwordError, this.state.confirmPasswordError]

        //If empty field
        if (credsList.includes('')) {
            return this.setState({
                mainError: true,
                mainErrorMessage: 'Please fill out all fields'
            })
        }

        //If there is an error
        if (errorsList.includes(true)) {
            return this.setState({
                mainError: true,
                mainErrorMessage: 'Please correct any errors'
            })
        }

        //Reset any errors and send user details to backend
        this.setState({
            mainError: false,
            mainErrorMessage: null,
        }, () => {
            this.createNewUser();
        })
    }

    //Check name for value
    checkFirstName = () => {
        const firstName = this.state.firstName

        //Check if empty
        if (isEmpty(firstName, { ignore_whitespace: true })) {
            return this.setState({
                firstNameError: true,
                firstNameErrorMessage: 'Cannot be empty'
            });
        }

        //Reset error message
        return this.setState({
            firstNameError: false,
            firstNameErrorMessage: null
        });
    }

    //Check last name
    checkLastName = () => {
        const lastName = this.state.lastName

        //Check if empty
        if (isEmpty(lastName, { ignore_whitespace: true })) {
            return this.setState({
                lastNameError: true,
                lastNameErrorMessage: 'Cannot be empty'
            });
        }

        //Reset error message
        return this.setState({
            lastNameError: false,
            lastNameErrorMessage: null
        });

    }

    //Check Email
    checkEmail = () => {
        const email = this.state.email

        //Check if empty
        if (isEmpty(email, { ignore_whitespace: true })) {
            return this.setState({
                emailError: true,
                emailErrorMessage: 'Cannot be empty'
            })
        }

        //If invalid email
        if (!isEmail(email)) {
            return this.setState({
                emailError: true,
                emailErrorMessage: 'Invalid email address'
            })
        }

        //Reset error message
        this.setState({
            emailError: false,
            emailErrorMessage: null,
        })
    }

    //Check password on Blur
    checkPassword = () => {
        const password = this.state.password

        //Password too short
        if (password.length < 8) {
            return this.setState({
                passwordError: true,
                passwordErrorMessage: 'Password must be 8 characters'
            })
        }

        //Reset password length
        return this.setState({
            passwordError: false,
            passwordErrorMessage: null
        })

    }

    //Check cofirm password on Blur
    checkConfirmPassword = () => {
        const password = this.state.password
        const confirmPassword = this.state.confirmPassword

        //Check if passwords match
        if (confirmPassword !== password) {
            return this.setState({
                confirmPasswordError: true,
                confirmPasswordErrorMessage: 'Passwords do not match'
            })
        }

        //Reset error message
        return this.setState({
            confirmPasswordError: false,
            confirmPasswordErrorMessage: null,
        })

    }

    //Handle when input is used
    handleChange = event => {
        const name = event.target.name
        const value = event.target.value

        //Set the state to the value of the input element
        this.setState({
            [name]: value
        })
    }

    handleClick = event => {
        event.preventDefault();

        this.validateForms()
    }

    componentDidMount() {
        //Load in email from query params
        const params = queryString.parse(window.location.search)

        //Check email from landing card redirect
        if (params.email !== null && params.email !== "") {
            this.setState({ email: params.email }, () => {
                this.checkEmail()
            })
        }
    }

    render() {
        const mainErrorMessage = <div className="loginErrorMessage">{this.state.mainErrorMessage}</div>

        return (
            <div className="signUpCard">
                <h1 className="signUpHeader">Create an account</h1>

                {this.state.mainError ? mainErrorMessage : null}

                <form className='signUpForm' name='signUpForm'>
                    <div className="signUpInputContainer">
                        <div style={{ width: '49%' }}>
                            <input name='firstName' onChange={this.handleChange} onBlur={this.checkFirstName} className='signUpInput' type='text' placeholder='First Name' />
                            <div className="signUpSingleError">{this.state.firstNameError ? this.state.firstNameErrorMessage : null}</div>
                        </div>

                        <div style={{ width: '49%' }}>
                            <input name='lastName' onChange={this.handleChange} onBlur={this.checkLastName} className='signUpInput' type='text' placeholder='Last Name' />
                            <div className="signUpSingleError">{this.state.lastNameError ? this.state.lastNameErrorMessage : null}</div>
                        </div>
                    </div>

                    <div className="signUpInputContainer">
                        <input name='email' value={this.state.email} onChange={this.handleChange} onBlur={this.checkEmail} className='signUpInput' type='text' placeholder='Email Address' />
                        <div className="signUpSingleError">{this.state.emailError ? this.state.emailErrorMessage : null}</div>
                    </div>

                    <div className="signUpInputContainer">
                        <input name='password' onChange={this.handleChange} onBlur={this.checkPassword} className='signUpInput' type='password' placeholder='Password' />
                        <div className="signUpSingleError">{this.state.passwordError ? this.state.passwordErrorMessage : null}</div>
                    </div>

                    <div className="signUpInputContainer">
                        <input name='confirmPassword' onChange={this.handleChange} onBlur={this.checkConfirmPassword} className='signUpInput' type='password' placeholder='Confirm Password' />
                        <div className="signUpSingleError">{this.state.confirmPasswordError ? this.state.confirmPasswordErrorMessage : null}</div>
                    </div>

                    <div>
                        <button onClick={this.handleClick} disabled={this.state.buttonDisabled} className='signUpButton' type='button'>
                            {this.state.sendingRequest ? <Facebook size={24} color={"#FFFFFF"} /> : 'Sign up'}
                        </button>
                    </div>
                </form>

            </div>
        )
    }
}