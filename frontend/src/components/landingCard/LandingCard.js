import React from 'react';
import './LandingCard.css';
import { Link } from 'react-router-dom';
import { Particles } from 'react-particles-js';

export class LandingCard extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            email: null,
            particleAmount: null
        }
    }

    handleChange = event => {
        const inputVal = event.target.value;

        this.setState({ email: inputVal })
    }

    //Update particle amount based on screen size
    handleScreenSize = () => {
        const screenWidth = window.innerWidth
        let particleAmount

        if (screenWidth < 500) {
            particleAmount = 50
        } else if (screenWidth < 900) {
            particleAmount = 70
        } else {
            particleAmount = 100
        }

        this.setState({ particleAmount })
    }

    componentDidMount() {
        //Get screen size and choose particle amount
        this.handleScreenSize();
    }

    render() {
        const hasAuth = this.props.hasAuth

        if (hasAuth) {
            const firstName = this.props.firstName

            //Logged in
            return (
                <div className="landingCardContainer">
                    <h1>Hello {firstName}</h1>
                    <div>Are you ready to learn everything there is to know about cryptocurrency?</div>
                </div>
            )
        } else {

            const signUpUrl = '/signup?email=' + this.state.email
            //Not logged in
            return (
                <div className="landingCardContainer">
                    
                    <Particles 
                        params={{
                            particles: {
                                number: {
                                    value: this.state.particleAmount,
                                },
                                opacity: {
                                    value: .8,
                                    random: true,
                                },
                                line_linked: {
                                    opacity: .2,
                                    width: 2,
                                },
                                move: {
                                    random: true,
                                    out_mode: "out"
                                }
                            },
                            interactivity: {
                                events: {
                                    onresize: {
                                        enable: true,
                                        density_auto: true,
                                    }
                                }
                            }
                        }}
                        style={{ color: "rgba(255, 255, 0, .01)"}} height={700} canvasClassName="particle-canvas" />
                    
                    <h1>Learn, buy, and trade cryptocurrency</h1>
                    <div>Sign up and get up to date information on everything cryptocurrency</div>

                    <br />

                    <input onChange={this.handleChange} type="text" className="landingCardEmailInput" placeholder="Email Address" />

                    <Link to={signUpUrl}>
                        <button className="landingCardJoinButton">Let's Go</button>
                    </Link>

                    <div className="landingCardIconContainer">
                        <img className="landingCardIcon" src="/blockchain.png" alt="blockchain" />
                        <img className="landingCardIcon" src="/wallet.png" alt="crypto wallet" />
                        <img className="landingCardIcon" src="/rocket.png" alt="rocket" />
                    </div>
                </div>
            )
        }
    }
}