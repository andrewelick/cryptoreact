import React from 'react';
import { Link } from 'react-router-dom';
import { isEmpty, isEmail } from 'validator';
import './login.css'

export class LoginCard extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            email: '',
            password: '',
            error: false,
            errorMessage: ''
        }
    }

    //Check if email and password was correct
    signInRequest = () => {
        const email = this.state.email
        const password = this.state.password

        //Check if empty
        if (isEmpty(email) || isEmpty(password)) {
            return this.setState({
                error: true,
                errorMessage: 'Please fill out all fields'
            });
        }
        
        //Check email format
        if (!isEmail(email)) {
            return this.setState({
                error: true,
                errorMessage: 'Email is not a valid format'
            });
        }
        
        //Clear error messages
        this.setState({
            error: false,
            errorMessage: '',
        })

        //Url of API endpoint
        const url = 'https://blockfade.com/api/user/login'; //https://blockfade.com/api/user/login

        //Send request to API
        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email,
                password
            })
        })
            .then(response => response.json())
            .then(response => {
                console.log(response)
                //If account credentials were incorrect
                if (response.error) {
                    return this.setState({
                        error: true,
                        errorMessage: 'Invalid Email/Password combination, please try again'
                    })
                }

                //Save user details to local storage
                localStorage.setItem('userDetails', JSON.stringify(response));

                return this.props.handleAuth()
            })
            .catch(error => {
                console.log(error)
                
                return this.setState({
                    error: true,
                    errorMessage: 'There was an error connecting to our services, please try again'
                })
            });
    }

    //Handle when input is used
    handleChange = event => {
        const name = event.target.name
        const value = event.target.value

        //Set the state to the value of the input element
        this.setState({
            [name]: value
        })
    }

    handleClick = event => {
        event.preventDefault();

        this.signInRequest()
    }


    render() {
        const errorMessage = <div className="loginErrorMessage">{this.state.errorMessage}</div>

        return (
            <div className="loginCard">

                <h1 className="loginHeader">
                    <span className="loginWebsiteName">Block<span className="loginWebsiteNameThin">Fade</span></span>
                    <span> sign in</span>
                </h1>

                {this.state.error ? errorMessage : null}

                <form className='loginForm' name='loginForm'>
                    <div>
                        <input name='email' value={this.state.email} onChange={this.handleChange} className='loginInput' type='text' placeholder='Email' />
                    </div>

                    <div>
                        <input name='password' value={this.state.password} onChange={this.handleChange} className='loginInput' type='password' placeholder='Password' />
                    </div>

                    <Link to='/forgot'>
                        <span className='loginForgotLink'>Forgot Password?</span>
                    </Link>

                    <div>
                        <button name='loginButton' onClick={this.handleClick} className='loginButton'>Sign In</button>
                    </div>
                </form>

            </div>
        )
    }
}