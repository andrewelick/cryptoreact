import React from 'react';
import { BuyCoinsItem } from './BuyCoinsItem';

import './buy.css'

export class BuyCoinsList extends React.Component {
    constructor(props) {
        super(props)

        this.topMarkets = [
            {
                id: 'gdax', //coinbase
                url: 'https://www.coinbase.com/join/elick_y',
            },
            {
                id: 'kraken',
                url: 'https://r.kraken.com/yAjeN',
            },
            {
                id: 'coinmama', //not listed
                url: 'https://go.coinmama.com/visit/?bta=59223&nci=5360',
            },
            {
                id: 'changenow', //not listed
                url: 'https://changenow.io?link_id=9ab1ba092de859',
            },
            {
                id: 'cex',
                url: 'https://cex.io/r/0/up129070888/0/',
            },
            {
                id: 'shapeshift', //not listed
                url: '',
            },
            {
                id: 'localbitcoin', //not listed
                url: 'https://localbitcoins.com/?ch=16hk6',
            },
            {
                id: 'abra', //not listed
                url: 'https://invite.abra.com/w7NZ4lwCG2'
            },
            {
                id: 'binance',
                url: 'https://www.binance.com/en/register?ref=P0N9XKE9',
            },
            {
                id: 'etoro', //not listed
                url: 'http://partners.etoro.com/A83966_TClick.aspx'
            },
            {
                id: 'changelly', //not listed
                url: 'https://changelly.com/?ref_id=r5md5umgcjwpx6ne'
            }
        ]

        this.state = {
            allLoaded: false,
        }
    }

    //Render each market item
    renderCoinList = () => {
        const marketList = this.topMarkets

        return marketList.map(market => {
            const { id, url } = market

            return <BuyCoinsItem key={id} coinSymbol={this.props.coinSymbol} marketId={id} marketUrl={url} />
        })
    }

    render() {
        return (
            <div className="buyCoinsList">
                <div className="buyCoinsListItemsContainer">
                    {this.renderCoinList()}
                </div>
            </div>
        )
    }
}