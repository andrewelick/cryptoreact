import React from 'react';
import { Facebook } from 'react-spinners-css';

import './buy.css'

export class BuyCoinsItem extends React.Component {
    abortController = new AbortController()

    constructor(props) {
        super(props)

        this.coinSymbol = this.props.coinSymbol
        this.marketId = this.props.marketId
        this.marketUrl = this.props.marketUrl

        this.notListedMarkets = [
            {
                id: 'coinmama',
                name: 'CoinMama',
                image: 'https://www.buybitcoinworldwide.com/img/exchanges/coinmama.png',
                trust_score: 9,
                country: 'Israel',
                year_established: 2013,
                supportedCoins: ['btc', 'eth', 'xrp', 'ltc', 'bch', 'ada', 'qtum', 'etc'],
            },
            {
                id: 'changenow',
                name: 'ChangeNow',
                image: 'https://lh3.googleusercontent.com/hgAq62Xb8-oHJvp0I0yXoCjxREmKyff6FVFxEeUq_YfV34b6EKvJM2flT53cGONvvjEG33q1gBUpML5ih656=w958-h930',
                trust_score: 7,
                country: 'Belize',
                year_established: 2017,
                supportedCoins: [],
            },
            {
                id: 'localbitcoin',
                name: 'LocalBitcoin',
                image: 'https://btxchange.io/wp-content/uploads/2018/10/localbitcoins-logo-2.png',
                trust_score: 9,
                country: 'Finland',
                year_established: 2012,
                supportedCoins: ['btc', 'eth', 'xrp', 'ltc']
            },
            {
                id: 'shapeshift',
                name: 'Shapeshift',
                image: 'https://info.shapeshift.io/wp-content/uploads/2019/03/medium_profile.png',
                trust_score: 8,
                country: 'Switzerland',
                year_established: 2014,
                supportedCoins: ['btc', 'bch', 'dash', 'dgb', 'doge', 'ltc', 'omg']
            },
            {
                id: 'etoro',
                name: 'Etoro',
                image: 'https://res-4.cloudinary.com/crunchbase-production/image/upload/c_lpad,h_120,w_120,f_auto,b_white,q_auto:eco/iclberphge7gnwbyqffl',
                trust_score: 9,
                country: 'United States',
                year_established: 2007,
                supportedCoins: ['btc', 'bch', 'eth', 'xrp', 'ltc', 'xlm']
            },
            {
                id: 'abra',
                name: 'Abra',
                image: 'https://pbs.twimg.com/profile_images/1135667498412662784/m-7dTmK2_400x400.png',
                trust_score: 9,
                country: 'United States',
                year_established: 2014,
                supportedCoins: ['btc', 'bch', 'dash', 'dgb', 'doge', 'ltc', 'zrx', 'rep', 'bat', 'btg', 'bsv', 'blk', 'gbyte', 'btm', 'ada', 'crw', 'cure', 'mana', 'dcr', 'xdn', 'edr', 'emc2', 'enj', 'eos', 'eth', 'etc', 'excl', 'exp', 'flo', 'ftc', 'game', 'geo', 'gnt', 'grs', 'nlg', 'zen', 'ioc', 'kmd', 'lbc', 'lsk', 'loom', 'meme', 'mer', 'mtl', 'mona', 'xmr', 'mue', 'xmy', 'nav', 'xem', 'neo', 'ok', 'omg', 'pax', 'ppc', 'pink', 'pivx', 'qtum', 'rads', 'rvn', 'rdd', 'xrp', 'sls', 'sc', 'sib', 'solve', 'spnd', 'sphr', 'usds', 'snt', 'xst', 'xlm', 'strat', 'sys', 'trx', 'ubq', 'ukg', 'xvg', 'vrc', 'vtc', 'via', 'waves', 'zec', 'xzc']
            },
            {
                id: 'changelly',
                name: 'Changelly',
                image: 'https://static.cryptorival.com/imgs/exchanges/changelly/logo-square.png',
                trust_score: 9,
                country: 'Czech Republic',
                year_established: 2015,
                supportedCoins: ['btc', 'bch', 'dash', 'dgb', 'doge', 'ltc', 'zrx', 'rep', 'bat', 'btg', 'bsv', 'blk', 'gbyte', 'btm', 'ada', 'crw', 'cure', 'mana', 'dcr', 'xdn', 'edr', 'emc2', 'enj', 'eos', 'eth', 'etc', 'excl', 'exp', 'flo', 'ftc', 'game', 'geo', 'gnt', 'grs', 'nlg', 'zen', 'ioc', 'kmd', 'lbc', 'lsk', 'loom', 'meme', 'mer', 'mtl', 'mona', 'xmr', 'mue', 'xmy', 'nav', 'xem', 'neo', 'ok', 'omg', 'pax', 'ppc', 'pink', 'pivx', 'qtum', 'rads', 'rvn', 'rdd', 'xrp', 'sls', 'sc', 'sib', 'solve', 'spnd', 'sphr', 'usds', 'snt', 'xst', 'xlm', 'strat', 'sys', 'trx', 'ubq', 'ukg', 'xvg', 'vrc', 'vtc', 'via', 'waves', 'zec', 'xzc']
            }
        ]
        this.state = {
            loading: true,
        }
    }

    //Get market details
    fetchMarketDetails = marketId => {
        const url = "https://api.coingecko.com/api/v3/exchanges/" + marketId

        fetch(url, { signal: this.abortController.signal })
            .then(response => {
                if (!response.ok) throw new Error("Not a listed market")

                return response
            })
            .then(response => response.json())
            .then(market => {
                const { name, image, trust_score, country, year_established } = market

                const supportedCoins = []

                //Get supported coins for the market
                market.tickers.map(coin => {
                    if (coin.base === "XBT") coin.base = 'BTC'

                    if (!supportedCoins.includes(coin.base)) return supportedCoins.push(coin.base.toLowerCase())

                    return null
                })

                this.setState({
                    name,
                    image,
                    trust_score,
                    country,
                    year_established,
                    supportedCoins,
                    loading: false,
                })
            })
            .catch(error => {
                const unlistedMarkets = ['coinmama', 'changenow', 'localbitcoin', 'shapeshift', 'abra', 'etoro', 'changelly']

                //If a unlisted market in coingecko
                if (unlistedMarkets.includes(this.marketId)) {

                    this.notListedMarkets.filter(market => {
                        if (market.id === this.marketId) {
                            const { name, image, trust_score, country, year_established, supportedCoins } = market

                            return this.setState({
                                name,
                                image,
                                trust_score,
                                country,
                                year_established,
                                supportedCoins,
                                loading: false,
                            });
                        } else {
                            return null
                        }
                    });
                }

                //If market is ChangeNow.io
                if (marketId === "changenow") {
                    this.fetchChangeNowCoins()
                }
            })
    }

    //fetch ChangeNow.io supportedCoins list
    fetchChangeNowCoins = () => {
        const url = "https://changenow.io/api/v1/currencies?active=true&fixedRate=false";
        fetch(url, { signal: this.abortController.signal })
            .then(response => response.json())
            .then(results => {
                const coinsList = []

                results.map(coin => {
                    return coinsList.push(coin.ticker)
                })

                //Add to ChangeNow object
                this.notListedMarkets.filter(market => {
                    if (market.id === "changenow") return this.setState({ supportedCoins: coinsList })

                    return null
                })
            })
            .catch(error => {
                console.log(error)
            })
    }

    componentDidMount() {
        this.fetchMarketDetails(this.marketId)
    }

    componentWillUnmount() {
        this.abortController.abort()
    }

    render() {
        if (this.state.loading) {
            return (
                <div className="buyCoinsItem">
                    <Facebook size={45} color="#3068b5" />
                </div>
            )
        }

        if (!this.state.loading) {
            //If not coin is not supported
            if (!this.state.supportedCoins.includes(this.coinSymbol)) {
                return null
            }

            const { name, image, trust_score, country, year_established } = this.state

            return (
                <div className="buyCoinsItem">
                    <div className="buyCoinsItemHeader">
                        <div>
                            <img className="buyCoinsItemImage" src={image} alt={name} />
                            <div className="buyCoinsItemName">{name}</div>
                        </div>

                        <a href={this.props.marketUrl} target="_blank" rel="noopener noreferrer">
                            <button className="buyCoinsItemButton">Buy</button>
                        </a>
                    </div>

                    <div className="buyCoinsItemBody">
                        <div className="buyCoinsItemBodyItem" >
                            <div className="buyCoinsItemBodyLabel">Country</div>
                            {country}
                        </div>
                        <div className="buyCoinsItemBodyItem" >
                            <div className="buyCoinsItemBodyLabel">Est.</div>
                            {year_established}
                        </div>
                        <div className="buyCoinsItemBodyItem" >
                            <div className="buyCoinsItemBodyLabel">Trust Score</div>
                            {trust_score}/10
                        </div>
                    </div>
                </div>
            )
        }

        return (
            <div className="buyCoinsItem">
                <div className="buyCoinsItemName">{this.marketId}</div>
            </div>
        )
    }
}