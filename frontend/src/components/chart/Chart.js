import React from 'react';
import { ResponsiveContainer, AreaChart, Area, XAxis, YAxis, Tooltip } from 'recharts';
import { CustomXAxisTick } from './customXAxisTick';
import { CustomToolTip } from './CustomToolTip';
import { Facebook } from 'react-spinners-css';
import 'moment-timezone';

export class TinyChart extends React.Component {
    abortController = new AbortController()

    constructor(props) {
        super(props);

        this.cryptoChartData = [];

        this.state = {
            chartData: null,
            dataLoaded: false,
            dateRange: this.props.dateRange,
        };
    }

    //Get Unix Timestamp from an hour ago
    getTimeStampHour = () => {
        const CurrentDate = new Date();
        const hourAgo = Math.floor((CurrentDate / 1000) - 3600);
        return hourAgo
    }

    //Get Unix Timestamp from a day ago
    getTimeStampDay = () => {
        const CurrentDate = new Date();
        const dayAgo = Math.floor((CurrentDate / 1000) - 86400);
        return dayAgo
    }

    //Get Unix Timestamp from 7 days ago
    getTimeStamp7Day = () => {
        const CurrentDate = new Date();
        const weekAgo = Math.floor((CurrentDate / 1000) - 604800);
        return weekAgo
    }

    //Get Unix TimeStamp from 30 days ago
    getTimeStamp30Day = () => {
        const CurrentDate = new Date();
        const monthAgo = Math.floor((CurrentDate / 1000) - 2629743);
        return monthAgo
    }

    //Get Unix Timestamp from an year ago
    getTimeStampYear = () => {
        const CurrentDate = new Date();
        const yearAgo = Math.floor((CurrentDate / 1000) - 31556926);
        return yearAgo
    }

    //Decide what function to use for user time selection
    handleDateChoice = dateRange => {
        switch (dateRange) {
            case '1h':
                return this.getTimeStampHour()
            case '24h':
                return this.getTimeStampDay()
            case '7d':
                return this.getTimeStamp7Day()
            case '30d':
                return this.getTimeStamp30Day()
            case '1y':
                return this.getTimeStampYear()
            default:
                return 'invalid time'
        }

    }

    //Choose granularity of price depending on price
    chooseGranularity = price => {

        //Show 4 decimals if sub dollar
        if (price < 1) {
            return Number(price.toFixed(4));
        }

        //Show 3 decimals if sub 10 dollars
        if (price < 10) {
            return Number(price.toFixed(3));
        }

        //Show only 2 decimals if greate than 10 dollars
        return Number(price.toFixed(2));
    }

    //Get data for chart
    updateMarketData = (coinId, dateRange) => {
        const currentTimeStamp = Date.now()
        const fromTimeStamp = this.handleDateChoice(dateRange)
        const url = "https://api.coingecko.com/api/v3/coins/" + coinId + "/market_chart/range?vs_currency=usd&from=" + fromTimeStamp + "&to=" + currentTimeStamp

        fetch(url, { signal: this.abortController.signal })
            .then(response => response.json())
            .then(data => {
                data.prices.map(details => {
                    return this.cryptoChartData.push({
                        price: this.chooseGranularity(details[1]),
                        time: details[0],
                    })
                })
            })
            .then(() => this.setState({
                dataLoaded: true,
                chartData: this.cryptoChartData,
            }))
            .then(this.cryptoChartData = [])
            .catch(error => console.log(error))
    }

    componentDidUpdate(nextprops) {
        //Update search result on change
        const { dateRange } = this.props
        if (nextprops.dateRange !== dateRange) {
            if (dateRange) {
                this.setState({ dateRange: dateRange }, () => {
                    this.updateMarketData(this.props.coinId, this.state.dateRange)
                })
            }
        }
    }

    componentDidMount() {
        this.updateMarketData(this.props.coinId, this.state.dateRange)
    }

    componentWillUnmount() {
        this.abortController.abort()
    }

    render() {
        if (this.state.dataLoaded === false) {
            return (
                <div style={{ textAlign: "center" }}>
                    <Facebook color='#FFFFFF' size='40px' />
                </div>
            )
        } else {
            return (
                <ResponsiveContainer width="100%" height="100%">
                    <AreaChart data={this.state.chartData} margin={{ left: -60, bottom: -10 }} style={{ cursor: "crosshair" }}>
                        <XAxis dataKey="time" domain={['dataMin', 'dataMax']} axisLine={false} tickSize={0} tick={<CustomXAxisTick dateRange={this.state.dateRange} />} interval={"preserveStartEnd"} />
                        <YAxis domain={['auto', 'dataMax']} tick={false} axisLine={false} />
                        <Area type="monotone" dataKey="price" dot={false} stroke="#FFFFFF" fill="url(#fillColor)" strokeWidth={2} />
                        <Tooltip itemStyle={{ color: '#383838' }} contentStyle={{ borderRadius: '5px', fontSize: '13px' }} allowEscapeViewBox={{ x: false, y: true }} cursor={false} content={<CustomToolTip dateRange={this.state.dateRange} />} />

                        <defs>
                            <linearGradient id="fillColor" x1="0" y1="0" x2="0" y2="1">
                                <stop offset="5%" stopColor="#86abdf" stopOpacity={0.8} />
                                <stop offset="90%" stopColor="#86abdf" stopOpacity={0.02} />
                            </linearGradient>
                        </defs>
                    </AreaChart>
                </ResponsiveContainer>

            )
        }
    }
}