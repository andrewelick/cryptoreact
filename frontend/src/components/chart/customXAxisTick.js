import React from 'react';
import 'moment-timezone';
import moment from 'moment';

export class CustomXAxisTick extends React.Component {

    //Format unix timestamp to desired time stamp display
    tickFormat = (timestamp) => {
        const dateRange = this.props.dateRange

        switch (dateRange) {
            case '1h':
                return moment(timestamp).format('LT');
            case '24h':
                return moment(timestamp).format('LT');
            case '7d':
                return moment(timestamp).format('MMM Do');
            case '30d':
                return moment(timestamp).format('MMM Do');
            case '1y':
                return moment(timestamp).format('ll');
            default:
                return 'invalid time'
        }

    }

    render() {
        const {
            x, y, payload,
        } = this.props;

        return (
            <g transform={`translate(${x - 20},${y})`}>
                <text x={0} y={0} dy={15} fontSize={12} textAnchor="start" fill="#666666">{this.tickFormat(payload.value)}</text>
            </g>
        );
    }
}