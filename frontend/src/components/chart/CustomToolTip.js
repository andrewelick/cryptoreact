import React from 'react';
import 'moment-timezone';
import moment from 'moment';
import './chart.css'

export class CustomToolTip extends React.Component {

    //Format unix timestamp to desired time stamp display
    timeFormat = (timestamp) => {
        const dateRange = this.props.dateRange

        switch (dateRange) {
            case '1y':
                return moment(timestamp).format('ll');
            default:
                return moment(timestamp).format('lll');
        }

    }

    render() {
        const { active, payload, label } = this.props
        if (active) {
            return (
                <div className="custom-tooltip">
                    <div className="toolTipPrice">${payload[0].value}</div>
                    <div className="toolTipTime">{this.timeFormat(label)} </div>
                </div>
            );
        }

        return null;
    }
}