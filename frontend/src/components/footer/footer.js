import React from 'react';
import { Link } from 'react-router-dom';
import './footer.css'

export class Footer extends React.Component {

    render() {
        return (
            <div className="footerContainer">
                <span className="footerWebsite">
                    Block<span className="footerWebsiteThin">Fade</span>
                </span>

                <Link to='/'>
                    <span className="footerItem">Home</span>
                </Link>

                <Link to='/contact'>
                    <span className="footerItem" >Contact</span>
                </Link>

                <Link to='/prices'>
                    <span className="footerItem" >Prices</span>
                </Link>

                <Link to='/news'>
                    <span className="footerItem" >News</span>
                </Link>

            </div>
        )
    }
}