import React from 'react';
import './prices.css';
import { SinglePrice } from './singlePrice';
import { Facebook } from 'react-spinners-css';
import InfiniteScroll from 'react-infinite-scroller';

export class PricesList extends React.Component {
    abortController = new AbortController()

    constructor(props) {
        super(props)

        this.state = {
            coins: [],
            searchedCoins: [],
            searchResult: null,
            timeRange: this.props.selectedTime,
            coinAmount: 20,
        }

        this.handleAPICall = this.handleAPICall.bind(this)
    }

    //Make API request to CoinGecko
    handleAPICall = (url, method) => {
        return new Promise((resolve, reject) => {
            fetch(url, {
                signal: this.abortController.signal,
                method: method,
            })
                .then(response => response.json())
                .then(response => {
                    return resolve(response)
                })
                .catch(error => {
                    return reject(error)
                })
        });
    }

    //Get top 100 coins by Market Cap
    getTopCoins = async () => {
        const url = "https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=100&page=1&sparkline=false&price_change_percentage=1h%2C24h%2C7d%2C30d%2C1y"
        const method = "get"

        //Make API request
        const topCoins = await this.handleAPICall(url, method, null)

        //If request error
        if (topCoins.error) {
            console.log(topCoins)
        }

        //Update state
        this.setState({ coins: topCoins })
    }

    //Load 20 more coins to DOM
    loadMoreCoins = () => {
        const currentCount = this.state.coinAmount
        const addCount = 20

        if (currentCount >= 100) {
            return false
        }
        return this.setState({ coinAmount: currentCount + addCount })
    }

    //Filter coin results
    filterCoinResults = searchResult => {
        const filteredCoins = []

        //Filter coins by name
        this.state.coins.filter(coin => {
            if (coin.name.toLowerCase().includes(searchResult)) {
                return filteredCoins.push(coin)
            } else {
                return null
            }
        })

        return this.setState({ searchedCoins: filteredCoins })
    }

    componentDidUpdate(nextprops) {
        //Update search result on change
        const { searchResult } = this.props

        if (nextprops.searchResult !== searchResult) {
            if (searchResult) {
                this.setState({ searchResult: searchResult })
                this.filterCoinResults(searchResult)
            } else {
                this.setState({
                    searchResult: null,
                    searchedCoins: [],
                    coinAmount: 20,
                })
            }
        }
    }

    componentDidMount() {
        //Load top coins
        this.getTopCoins()

        //Update prices every minute
        this.interval = setInterval(() => {
            this.getTopCoins()
        },
            60000
        )
    }

    componentWillUnmount() {
        //clear interval after unmounting
        clearInterval(this.interval)

        this.abortController.abort()
    }

    render() {
        let renderPrices

        //If search and no results
        if (this.state.searchResult !== null && this.state.searchedCoins.length === 0) {
            renderPrices = <div className="pricesListNoResults">No results found</div>
        } else if (this.state.searchResult !== null) {
            //Render each coin
            renderPrices = this.state.searchedCoins.map(coin => {
                return <SinglePrice key={coin.id} coinInfo={coin} selectedTime={this.props.selectedTime} />
            })
        } else {
            //Limited amount of coins shown
            const limitedCoinsArray = this.state.coins.slice(0, this.state.coinAmount, (coin) => {
                return coin
            })

            //Render each coin
            renderPrices = limitedCoinsArray.map(coin => {
                return <SinglePrice key={coin.id} coinInfo={coin} selectedTime={this.props.selectedTime} />
            })
        }

        //Infinite scroller
        const showMoreButton = <InfiniteScroll key={0} pagestart={0} loadMore={this.loadMoreCoins} hasMore={true} loader={<Facebook key={0} color="#FFFFFF" />}>0</InfiniteScroll>

        return (
            <div className="pricesList">
                <div className="pricesListHeader">
                    <span className='pricesListHeaderItem'>Rank</span>
                    <span className='pricesListHeaderItem headerItemName'>Name</span>
                    <span className='pricesListHeaderItem'>Price</span>
                    <span className='pricesListHeaderItem'>Change</span>
                    <span className='pricesListHeaderItem'>Market Cap</span>
                </div>

                {this.state.coins.length > 0 ? renderPrices : <Facebook color='#FFFFFF' />}

                <div className='pricesListLoadMore'>
                    {this.state.coinAmount < this.state.coins.length ? showMoreButton : null}
                </div>
            </div>
        )
    }
}