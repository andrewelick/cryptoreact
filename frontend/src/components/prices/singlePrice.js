import React from 'react';
import { Link } from 'react-router-dom';
import './prices.css'
import Numeral from 'numeral';

export class SinglePrice extends React.Component {

    //Choose granularity depending on price
    chooseGranularity = price => {
        //Convert price to number
        price = Number(price)

        //Show 4 decimals if sub dollar
        if (price < .1) {
            return Numeral(price).format('$0.0000');
        }

        //Show 3 decimals if sub 10 dollars
        if (price < 1) {
            return Numeral(price).format('$0.000');;
        }

        //Show only 2 decimals if greate than 10 dollars
        return Numeral(price).format('$0.00');
    }

    render() {
        const coinInfo = this.props.coinInfo;

        const id = coinInfo.id;
        const rank = coinInfo.market_cap_rank;
        const name = coinInfo.name;
        const symbol = coinInfo.symbol.toUpperCase();
        const imgSrc = coinInfo.image;
        const currentPrice = this.chooseGranularity(coinInfo.current_price);
        const marketCap = Numeral(coinInfo.market_cap).format('$0.0a');

        //Format percentage change
        const percentChangeVal = 'price_change_percentage_' + this.props.selectedTime + '_in_currency';
        let percentChange = Number(coinInfo[percentChangeVal]).toFixed(2);

        //URL
        const coinIdURL = "/coin/" + id

        return (
            <Link to={coinIdURL}>
                <div className="singleCoin">
                    <span className="singleCoinRank">{rank}.</span>

                    <div className="coinDetails">
                        <img className="singleCoinPicture" src={imgSrc} alt={id} />
                        <span className="singleCoinName">{name}</span>
                        <span className="singleCoinSymbol">({symbol})</span>
                    </div>

                    <div className="singleCoinPrice">{currentPrice}</div>

                    <span className={percentChange > 0 ? 'singleCoinChangePositive' : 'singleCoinChangeNegative'}>{percentChange}%</span>

                    <span className="singleCoinMarketCap">{marketCap}</span>
                </div>
            </Link>
        )
    }
}