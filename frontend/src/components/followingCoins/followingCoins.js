import React from 'react';
import { SingleFollowingCoin } from './singleFollowingCoin';
import '../../pages/css/homePage.css';

export class FollowingCoins extends React.Component {
    abortController = new AbortController()

    constructor(props) {
        super(props)

        this.state = {
            chosenCoins: [],
            oldChosenCoins: [],
            topCoins: [],
            searchedCoins: [],
            searchResult: '',
            editFollowingCoins: false,
        }
    }

    //Load following coins
    loadFollowingCoins = userId => {
        const url = "https://blockfade.com/api/followingCoins/coins?userId=" + userId;
        
        fetch(url)
            .then(response => response.json())
            .then(response => {
                this.setState({
                    chosenCoins: response.coins
                })
            })
            .catch(error => {
                console.log(error)
            })
    }

    //Select a coin for users custom homepage
    addFollowingCoin = coin => {
        let currentChosen = [...this.state.chosenCoins];
        
        //Prevent duplicate from adding to list
        if (currentChosen.filter(chosenCoin => chosenCoin.id === coin.id).length > 0) {
            return false;
        } else if (currentChosen.length >= 10) {
            return false;
        } else {
            //Add to list if not in it
            currentChosen.push(coin);

            //Add to state and clear search results
            this.setState({
                chosenCoins: currentChosen,
                searchResult: '',
                searchedCoins: []
            });
        }
    }

    //Remove from following list
    removeFollowingCoin = id => {
        const currentFollowingList = [...this.state.chosenCoins];
        const newFollowingList = currentFollowingList.filter(coin => {
            return coin.id !== id
        });

        this.setState({ chosenCoins: newFollowingList })
    }

    //Make API request to CoinGecko get top 100 coins by Market Cap
    getTopCoins = () => {
        return new Promise((resolve, reject) => {
            const url = "https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=100&page=1&sparkline=false"
            const method = "get"

            fetch(url, {
                signal: this.abortController.signal,
                method: method,
            })
                .then(response => response.json())
                .then(response => {
                    const formattedCoins = [];

                    //Format coins json structure to match our DB
                    response.map(coin => {
                        return formattedCoins.push({
                            id: coin.id,
                            name: coin.name,
                            image: coin.image
                        })
                    })

                    //Save to topCoins in state
                    this.setState({
                        topCoins: formattedCoins,
                        searchedCoins: formattedCoins.sort(function() { return 0.5 - Math.random()}).slice(0,10) //Sort and shuffle for random coins on load
                    });

                    return resolve(true)
                })
                .catch(error => {
                    return reject(error)
                })
        });
    }

    //Filter results of coins
    handleChange = event => {
        const searchResult = event.target.value;
        //Set user search results in state to use for filtering
        this.setState({ searchResult });
        //Filter coins using search result
        this.filterCoinResults(searchResult);
    }

    //Filter coin results
    filterCoinResults = searchResult => {
        const filteredCoins = []

        //Filter coins by name
        this.state.topCoins.filter(coin => {
            if (coin.name.toLowerCase().includes(searchResult)) {
                return filteredCoins.push(coin)
            } else {
                return null
            }
        })

        return this.setState({ searchedCoins: filteredCoins })
    }

    //Update state to notify updating of user following tokens
    handleEditFollowingClick = () => {
        if (this.state.editFollowingCoins) {
            const { chosenCoins, oldChosenCoins} = this.state;

            //If chosen coins were edited
            if (chosenCoins !== oldChosenCoins) {
                //Save following coins to database
                this.updateFollowingCoins();
            }
        }
        
        //Update the state to opposite of current
        this.setState({
            editFollowingCoins: !this.state.editFollowingCoins,
            oldChosenCoins: this.state.chosenCoins,
        });
    }

    //Save user following coins to database if they changed it
    updateFollowingCoins = () => {
        const savedFollowingCoins = this.state.chosenCoins;
        const formattedSavedCoins = []

        //Format saved coins data for database
        savedFollowingCoins.map(coin => {
            const coinDetails = {
                id: coin.id,
                name: coin.name,
                image: coin.image,
            }

            formattedSavedCoins.push(coinDetails)
        });
        
        //Send API request
        const requestURL = "https://blockfade.com/api/followingCoins/update";
        const method = "PUT";
        const body = JSON.stringify({
            userId: this.props.userId,
            coins: formattedSavedCoins
        });

        fetch(requestURL, {
            method,
            headers: { 'Content-Type': 'application/json' },
            body
        })
            .then(response => response.json())
            .then(response => {
                this.updateCoinCards();
            })
            .catch(error => {
                console.log("ERROR HAPPENED");
                console.log(error)
            })
    }

    //Update coincards after following coins were saved
    updateCoinCards = () => {
        this.props.refreshChosenCoins(this.props.userId);
    }

    componentDidMount() {
        //Fetch top 100 coins for search results
        this.getTopCoins();

        //If userId has been passed in loading their following coins
        if (this.props.userId) {
            this.loadFollowingCoins(this.props.userId);
        }
    }

    componentWillUnmount() {
        this.abortController.abort()
    }

    render() {
        return (
            <div className="chooseCoinContainer">
                {/*Following header and edit and save button*/}
                <div style={{ color: "#FFFFFF", width: "100%", paddingBottom: "10px", fontSize: "20px", fontWeight: 400, display: "flex", justifyContent: "space-between", alignItems: "center" }}>
                    <span>
                        Following
                        <span style={{ paddingLeft: "10px", fontSize: "13px" }}>({this.state.chosenCoins.length} of 10)</span>
                    </span>

                    <button className="chooseCoinButton" onClick={this.handleEditFollowingClick}>{this.state.editFollowingCoins ? "Done" : "Edit"}</button>
                </div>

                {/*Show following coins*/}
                {
                    this.state.chosenCoins.map(coin => {
                        return <SingleFollowingCoin key={"following-"+ coin.id} removeFollowingCoin={this.removeFollowingCoin} id={coin.id} image={coin.image} isEditing={this.state.editFollowingCoins} />
                    })
                }

                {/*Show coin following search bar and their results when editing */}
                {
                    this.state.editFollowingCoins ?
                    <React.Fragment>

                        <h5 style={{ color: "#FFFFFF", fontWeight: "400" }}>Start typing and choose the cryptocurrencies you want to follow</h5>
                        <input className="chooseCoinSearch" type="text" onChange={this.handleChange} value={this.state.searchResult} placeholder="Search for a cryptocurrency" />

                        {
                            this.state.searchedCoins.slice(0, 20).map(coin => {
                                return (
                                    <div key={"searchFollowing-"+ coin.id} className="chooseCoinSingleWrapper">
                                        <div onClick={() => this.addFollowingCoin(coin)} className="coinCardChooseOverlay" ></div>
                                        <img src={coin.image} height={30} />
                                        <div>{coin.id}</div>
                                    </div>
                                )
                            })
                        }
                    </React.Fragment>
                    :
                    null
                }
            </div>
        )
    }
}
