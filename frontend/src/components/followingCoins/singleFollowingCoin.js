import React from 'react';
import '../../pages/css/homePage.css';

const SingleFollowingCoin = props => {
    const { id, image, isEditing } = props;

    //Remove coin from following coins list
    const handleClick = () => {
        props.removeFollowingCoin(id);
    }

    return (
        <div className="chooseCoinSingleWrapper" style={{ minWidth: "0", minHeight: "0" }}>
            {
                isEditing ?
                <div onClick={() => handleClick(id)} className="coinCardChooseOverlay" ></div>
                :
                null
            }
            
            <img src={image} width={35} />
        </div>
    )
}

export { SingleFollowingCoin };