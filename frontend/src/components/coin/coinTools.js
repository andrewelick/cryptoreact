import React from 'react';
import { PriceConvertor } from '../tools/priceConvertor';
import { BuyCoinsList } from '../buy/BuyCoinsList';

import './coin.css';

export class CoinTools extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            tabActive: 'Buy'
        }

        this.tabOptions = ['Buy', 'Convert']
    }

    //Show active tab content
    renderOpenTab = () => {
        switch (this.state.tabActive) {
            case 'Convert':
                return (
                    <div>
                        <p style={{ fontSize: '13px', paddingBottom: '20px' }}>Quickly see how much your {this.props.coinId} is worth in fiat</p>
                        <PriceConvertor coinId={this.props.coinId} coinSymbol={this.props.coinSymbol} />
                    </div>
                )
            default:
                return (
                    <div>
                        <p style={{ fontSize: '13px', paddingBottom: '20px' }}>Ready to start trading {this.props.coinId}? Here is some popular cryptocurrency markets you can use!</p>
                        <BuyCoinsList key={0} coinSymbol={this.props.coinSymbol} />
                    </div>
                )
        }
    }

    //Render all tabs
    renderTabOptions = () => {
        const activeTab = this.state.tabActive;

        return this.tabOptions.map(option => {
            //Add active state to tab
            if (option === activeTab) {
                return <div key={option} data-id={option} onClick={this.handleOpenTabClick} className={"coinToolsHeaderItem activeTab"}>{option}</div>
            }

            return <div key={option} data-id={option} onClick={this.handleOpenTabClick} className="coinToolsHeaderItem">{option}</div>
        })
    }

    handleOpenTabClick = event => {
        const tabVal = event.target.dataset.id;
        return this.setState({ tabActive: tabVal })
    }

    render() {
        return (
            <div className="coinToolsConvertor">
                <div className="coinToolsHeader">
                    {this.renderTabOptions()}
                </div>

                <div className="coinToolsBody">
                    {this.renderOpenTab()}
                </div>
            </div>
        )
    }
}