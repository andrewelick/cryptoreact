import React from 'react';

const Daily = props => {
    const { high_24hr, low_24hr } = props.data

    return (
        <React.Fragment>
            <span className="coinDailyItem">
                24hr High:
                <div className="coinDailyPrice">${high_24hr}</div>
            </span>
            <span className="coinDailyItem">
                24hr Low:
                <div className="coinDailyPrice">${low_24hr}</div>
            </span>
        </React.Fragment>
    )
}

export { Daily };