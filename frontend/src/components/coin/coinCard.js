import React from 'react';
import { CoinHeader } from './coinHeader';
import { CoinPrice } from './coinPrice';
import { Daily } from './coinDaily';
import { DateRangePicker } from '../dateRange/DateRangePicker';
import { TinyChart } from '../chart/Chart';
import { CoinBodyMarketData } from './coinBodyMarketData';
import { CoinBodyMarketInfo } from './coinBodyMarketInfo';
import { News } from '../news/news';
import { Facebook } from 'react-spinners-css';
import Numeral from 'numeral';

import './coin.css'

export class CoinCard extends React.Component {
    abortController = new AbortController()

    constructor(props) {
        super(props)

        this.state = {
            coinInfo: null,
            dateRange: '24h',
        }

        //Header component data
        this.headerData = {}

        //Price component data
        this.priceData = {}

        //Daily component data
        this.dailyData = {}

        //Market stats data
        this.marketStatsData = {}

        //Info component data
        this.infoData = {}

        //News
        this.newsData = {}
    }

    //Fetch coin information
    fetchCoinInfo = coinId => {
        const url = "https://api.coingecko.com/api/v3/coins/" + coinId + "?localization=false&tickers=false&market_data=true&community_data=true&developer_data=true&sparkline=true";

        return fetch(url, { signal: this.abortController.signal })
            .then(response => response.json())
            .then(response => {
                return this.setState({ coinInfo: response })
            })
            .catch(error => {
                console.log(error)
            })
    }

    //Change the time for percentage
    changeTime = timeVal => {
        return this.setState({ dateRange: timeVal })
    }

    //Choose granularity depending on price
    chooseGranularity = price => {
        //Convert price to number
        price = Number(price)

        //If price is negative
        if (price < 0) {
            //Show 4 decimals if sub dollar
            if (price > -.1) {
                return price.toFixed(4);
            }

            //Show 3 decimals if sub 10 dollars
            if (price > -1) {
                return price.toFixed(3);
            }

            //Show only 2 decimals if greater than 10 dollars
            return price.toFixed(2);
        }
        
        //Show 4 decimals if sub dollar
        if (price < .1) {
            return price.toFixed(4);
        }

        //Show 3 decimals if sub 10 dollars
        if (price < 1) {
            return price.toFixed(3);
        }

        //Show only 2 decimals if greater than 10 dollars
        return price.toFixed(2);
    }

    //Calculate percentage change
    getDollarChange = (currentPrice, changePercentage) => {
        const pastPrice = (currentPrice / ((changePercentage / 100) + 1));
        return currentPrice - pastPrice;
    }

    //Assign color for price change
    getPriceColor = percentChange => {
        if (percentChange < 0) {
            return 'coinPriceChange negativeColor'
        } else if (percentChange > 0) {
            return 'coinPriceChange positiveColor'
        } else {
            return 'coinPriceChange'
        }
    }

    componentDidMount() {
        //Fetch coin info
        this.fetchCoinInfo(this.props.coinId);

        //Update prices every minute
        this.interval = setInterval(() => {
            this.fetchCoinInfo(this.props.coinId);
        },
            60000
        )
    }

    componentWillUnmount() {
        this.abortController.abort()

        //clear interval after unmounting
        clearInterval(this.interval)
    }

    render() {
        const coinInfo = this.state.coinInfo
        
        //Coin data is loading
        if (!this.state.coinInfo) {
            return <Facebook color={"#FFFFFF"} size={40} />
        }

        //Header component visible
        if (this.props.visibility.header) {
            //Header variables
            const picture = this.state.coinInfo.image.small;
            const name = this.state.coinInfo.name;
            const symbol = this.state.coinInfo.symbol.toUpperCase();
                        
            this.headerData = {
                picture,
                name,
                symbol,
                id: this.props.coinId
            }
        }

        ///Price component visible
        if (this.props.visibility.price) {
            const price = this.chooseGranularity(this.state.coinInfo.market_data.current_price.usd);
            const changePercentageString = "price_change_percentage_" + this.state.dateRange + "_in_currency";
            const percentChange = this.state.coinInfo.market_data[changePercentageString].usd.toFixed(2);
            const change = this.chooseGranularity(this.getDollarChange(price, percentChange));
            const changeColor = this.getPriceColor(percentChange)

            this.priceData = {
                price,
                change,
                percentChange,
                changeColor
            }
        }

        //Daily component is visible
        if (this.props.visibility.daily) {
            const high_24hr = coinInfo.market_data.high_24h.usd;
            const low_24hr = coinInfo.market_data.low_24h.usd;

            this.dailyData = { high_24hr, low_24hr }
        }

        //Market stats component is visible
        if (this.props.visibility.marketStats) {
            //Market stats variables
            const marketCap = Numeral(coinInfo.market_data.market_cap.usd).format('0.00a');
            const marketCapChange = Numeral(coinInfo.market_data.market_cap_change_24h_in_currency.usd).format('$0.00a');
            const ath = Numeral(coinInfo.market_data.ath.usd).format('$0,0.00');
            const circulatingSupply = Numeral(coinInfo.market_data.circulating_supply).format('0.00a');
            const rank = Numeral(coinInfo.market_cap_rank).format('0o');
            const liquidScore = coinInfo.liquidity_score.toFixed(2);
            const interestVotesUp = Math.round(coinInfo.sentiment_votes_up_percentage);
            const interestVotesDown = Math.round(coinInfo.sentiment_votes_down_percentage);        
            
            this.marketStatsData = {
                marketCap,
                marketCapChange,
                ath,
                circulatingSupply,
                rank,
                liquidScore,
                interestVotesUp,
                interestVotesDown
            }
        }

        //Info component is visible
        if (this.props.visibility.info) {
            //About coin
            const description = coinInfo.description.en.replace(/(<([^>]+)>)/ig, "");

            //Coin community
            const redditSubs = Numeral(coinInfo.community_data.reddit_subscribers).format('0.00a');
            const redditUrl = coinInfo.links.subreddit_url;
            const twitterFollowers = Numeral(coinInfo.community_data.twitter_followers).format('0.00a');
            const twitterName = coinInfo.links.twitter_screen_name

            //Coin Developer stats
            const forks = coinInfo.developer_data.forks;
            const stars = coinInfo.developer_data.stars;
            const subs = coinInfo.developer_data.subscribers;
            const commits = coinInfo.developer_data.commit_count_4_weeks;

            //Coin resources
            const website = coinInfo.links.homepage[0];
            const repo = coinInfo.links.repos_url.github[0];
            const blockChain = coinInfo.links.blockchain_site[0];

            this.infoData = {
                description,
                redditSubs,
                redditUrl,
                twitterFollowers,
                twitterName,
                forks,
                stars,
                subs,
                commits,
                website,
                repo,
                blockChain
            }
        }

        //News component visible
        if (this.props.visibility.news) {
            const name = this.state.coinInfo.name;
            const symbol = this.state.coinInfo.symbol;

            this.newsData = { name, symbol }
        }

        return (
            <div className="coinCardContainer" style={this.props.style.container} >
                {this.props.visibility.header ?
                    <div className="coinHeaderContainer">
                        <CoinHeader data={this.headerData} />

                        {this.props.visibility.price ?
                            <div className="coinPrice">
                                <CoinPrice data={this.priceData} />
                            </div> : null
                        }
                    </div> : null
                }

                {this.props.visibility.daily ?
                    <div className="coinDailyContainer">
                        <Daily data={this.dailyData} />
                    </div> : null
                }

                {this.props.visibility.chart ?
                    <div className="coinCardChart" style={{ marginTop:"0px", paddingTop:"10px"}}>
                        <span className="coinCardDateRangePickerContainer">
                            <DateRangePicker changeTime={this.changeTime} /> 
                        </span>
                        
                        <TinyChart dateRange={this.state.dateRange} coinId={this.props.coinId} />
                    </div> : null
                }

                {this.props.visibility.marketStats ?
                    <div className="coinBodyMarketDataContainer">
                        <CoinBodyMarketData data={this.marketStatsData} />
                    </div> :
                    null
                }

                {this.props.visibility.info ? 
                    <div className="coinBodyMarketInfoContainer">
                        <CoinBodyMarketInfo data={this.infoData} />
                    </div> :
                    null
                }

                {this.props.visibility.news ?
                    <div className="coinBodyNews">
                        <h4>Top stories</h4>
                        <News storiesAmount="10" coinSymbol={this.newsData.symbol} coinName={this.newsData.name} />
                    </div> :
                    null
                }
            </div>
        )
    }
}

//Default props if unset
CoinCard.defaultProps = {
    visibility: {
        header: true,
        price: true,
        daily: false,
        dateRangePicker: false,
        chart: true,
        marketStats: false,
        info: false,
        news: false,
    },
    style: {
        container: {},
        header: {},
        chart: {},
    }
}