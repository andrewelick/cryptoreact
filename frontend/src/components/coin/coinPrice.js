import React from 'react';
import './coin.css'

const CoinPrice = props => {
    const { price, changeColor, change, percentChange } = props.data

    return (
        <React.Fragment>
            ${price}
            <div className={changeColor}>${change} ({percentChange}%)</div>
        </React.Fragment>
    )
}

export { CoinPrice };