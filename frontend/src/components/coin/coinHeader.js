import React from 'react';
import { Link } from 'react-router-dom';
import './coin.css';

const CoinHeader = props => {
    const { picture, name, symbol, id } = props.data
    const linkPath = "/coin/" + id;

    return (
        <React.Fragment>
            <Link to={linkPath}>
                <img className="coinHeaderItem coinHeaderItemPicture" src={picture} alt="coin logo" />
            </Link>
            <Link to={linkPath}>
                <div className="coinHeaderItem coinHeaderItemName">{name}</div> 
            </Link>
            <div className="coinHeaderItem coinHeaderSymbol">({symbol})</div>
        </React.Fragment>
    )
}

export { CoinHeader }