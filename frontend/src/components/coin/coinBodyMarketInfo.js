import React, { useState } from 'react';
import './coin.css';

const CoinBodyMarketInfo = props => {
    //Description visibility state
    const [ descriptionOpen, expandDescription ] = useState(false);

    //Data items from props
    const {
        name,
        description,
        redditUrl,
        redditSubs,
        twitterName,
        twitterFollowers,
        forks,
        stars,
        subs,
        commits,
        website,
        blockChain,
        repo
    } = props.data;

    //Changes state of descriptionOpen for about section to expand
    const handleClick = () => {
        expandDescription(!descriptionOpen);
    }

    return (
        <div className="coinBodyMarketInfo">
            <h4>About {name}</h4>
            <div className="coinBodyMarketDescription" style={descriptionOpen ? { height: "auto"} : { height: "100px"}}>{description}</div>
            <div className="coinBodyMarketDescriptionMoreButton" onClick={handleClick}>{descriptionOpen ? "Show Less" : "Show More"}</div>
            
            <h4>Community stats</h4>
            <div className="coinBodyMarketInfoCommunity">
                <div className="coinBodyMarketInfoCommunityItem">
                    <a href={redditUrl} target="_blank" rel="noopener noreferrer" >
                        <img className="coinBodyMarketInfoCommunityItemIcon" src="/reddit_icon.png" alt="reddit icon" />
                        {redditSubs}
                    </a>
                </div>
                <div className="coinBodyMarketInfoCommunityItem">
                    <a href={"https://www.twitter.com/" + twitterName} target="_blank" rel="noopener noreferrer">
                        <img className="coinBodyMarketInfoCommunityItemIcon" src="/twitter_icon.png" alt="twitter icon" />
                        {twitterFollowers}
                    </a>
                </div>
            </div>

            <div className="coinBodyMarketInfoDevContainer">
                <div className="coinBodyMarketInfoDevItem">
                    <span className="coinBodyMarketInfoDevItemTitle">Forks</span>
                    <div>{forks}</div>
                </div>
                <div className="coinBodyMarketInfoDevItem">
                    <span className="coinBodyMarketInfoDevItemTitle">Stars</span>
                    <div>{stars}</div>
                </div>
                <div className="coinBodyMarketInfoDevItem">
                    <span className="coinBodyMarketInfoDevItemTitle">Subs</span>
                    <div>{subs}</div>
                </div>
                <div className="coinBodyMarketInfoDevItem">
                    <span className="coinBodyMarketInfoDevItemTitle">Commits</span>
                    <div>{commits}</div>
                </div>
            </div>

            <h4>Resources</h4>
            <div className="coinBodyMarketInfoResource">
                <a href={website} target="_blank" rel="noopener noreferrer">
                    <img className="coinBodyMarketInfoResourceIcon" src="/website.png" alt="website" />
                    Offical website
                </a>
            </div>
            <div className="coinBodyMarketInfoResource">
                <a href={blockChain} target="_blank" rel="noopener noreferrer">
                    <img className="coinBodyMarketInfoResourceIcon" src="/tiny-blockchain.png" alt="blockchain" />
                    Blockchain Info
                </a>
            </div>
            <div className="coinBodyMarketInfoResource">
                <a href={repo} target="_blank" rel="noopener noreferrer">
                    <img className="coinBodyMarketInfoResourceIcon" src="/repository.png" alt="repo" />
                    Code Repository
                </a>
            </div>
        </div>
    )
}

export { CoinBodyMarketInfo };

/*
export class CoinBodyMarketInfo extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            expandDescription: false,
        }
    }

    //Expand description
    expandDescription = () => {
        this.setState(prevState => ({
            expandDescription: !prevState.expandDescription
        }));
    }

    render() {
        
    }
}
*/