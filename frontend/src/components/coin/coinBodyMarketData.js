import React from 'react';
import './coin.css';

const CoinBodyMarketData = props => {
    //Data items from props
    const {
        marketCap,
        marketCapChange,
        ath, 
        circulatingSupply, 
        rank,
        liquidScore,
        interestVotesUp,
        interestVotesDown,
    } = props.data;

    return (
        <div className="coinBodyMarketData">
            <div className="coinBodyMarketDataItem">
                <span className="coinBodyMarketDataItemTitle">Market Cap</span>
                <div>${marketCap}</div>
            </div>
            <div className="coinBodyMarketDataItem">
                <span className="coinBodyMarketDataItemTitle">Market Cap Change (24h)</span>
                <div>{marketCapChange}</div>
            </div>
            <div className="coinBodyMarketDataItem">
                <span className="coinBodyMarketDataItemTitle">All time High</span>
                <div>{ath}</div>
            </div>
            <div className="coinBodyMarketDataItem">
                <span className="coinBodyMarketDataItemTitle">Circulating Supply</span>
                <div>{circulatingSupply}</div>
            </div>
            <div className="coinBodyMarketDataItem">
                <span className="coinBodyMarketDataItemTitle">Rank</span>
                <div>{rank}</div>
            </div>
            <div className="coinBodyMarketDataItem">
                <span className="coinBodyMarketDataItemTitle">Liquidity Score</span>
                <div>{liquidScore}</div>
            </div>
            <div className="coinBodyMarketDataItem">
                <span className="coinBodyMarketDataItemTitle">Public Opinion</span>
                <div className="coinBodyMarketInfoCommunityItemSentimentBar">
                    <div className="SentimentPostitive" style={{ width: interestVotesUp + "%" }}>
                        {interestVotesUp}%
                        <img src="/thumbs-up.png" alt="thumbs up" className="sentimentIcon" />
                    </div>
                    <div className="SentimentNegative" style={{ width: interestVotesDown + "%" }}>
                        {interestVotesDown}%
                        <img src="/thumbs-down.png" alt="thumbs down" className="sentimentIcon" style={{ verticalAlign: 'middle' }} />
                    </div>
                </div>
            </div>
        </div>
    )
}

export { CoinBodyMarketData };