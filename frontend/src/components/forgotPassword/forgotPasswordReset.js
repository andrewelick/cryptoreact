import React from 'react';
import { isEmpty } from 'validator';
import { Facebook } from 'react-spinners-css';
import './forgotPassword.css';

export class ForgotPasswordReset extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            invalidToken: false,
            passwordVal: '',
            confirmPasswordVal: '',
            sendingRequest: false,
            passwordResetSuccess: false,
            error: false,
            message: '',
        }
    }

    //Check password reset token
    validateResetToken = resetToken => {
        const requestUrl = "https://blockfade.com/api/forgotPassword/check";
        const bearerHeader = 'Basic ' + this.props.resetToken

        //Send request to API
        fetch(requestUrl, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': bearerHeader,
            }
        })
            .then(response => response.json())
            .then(response => {
                if (response.error) throw response
                return true
            })
            .catch(error => {
                console.log(error)
                //To disable bad token warning
                return this.setState({ invalidToken: true })
            })
    }

    //Update emailValue state when user enters email
    handlePasswordChange = event => {
        const passwordVal = event.target.value
        this.setState({ passwordVal })

        if (this.state.passwordVal.length > 7) {
            //Reset error state
            return this.setState({
                error: false,
                message: '',
            });
        }
    }

    //Update emailValue state when user enters email
    handleConfirmPasswordChange = event => {
        const confirmPasswordVal = event.target.value
        this.setState({ confirmPasswordVal })
    }

    checkPasswordLength = () => {
        if (this.state.passwordVal.length < 8) {
            return this.setState({
                error: true,
                message: 'Password must be at least 8 characters long'
            })
        }
    }

    //Validate passwords and send of request
    handleClick = () => {
        //To show loading indicator
        this.setState({
            sendingRequest: true,
            message: '',
        });

        const passwordVal = this.state.passwordVal;
        const confirmPasswordVal = this.state.passwordVal;
        
        //If either input is empty
        if (isEmpty(passwordVal) || isEmpty(confirmPasswordVal)) {
            return this.setState({
                sendingRequest: false,
                error: true,
                message: "Please fill out both inputs"
            });
        }

        //If password is not long enough
        if (passwordVal < 8) {
            return this.setState({
                sendingRequest: false,
                error: true,
                message: "Password must be at least 8 characters long"
            });
        }

        //If inputs do not match
        if (passwordVal !== confirmPasswordVal) {
            return this.setState({
                sendingRequest: false,
                error: true,
                message: "Passwords do not match"
            });
        }

        const requestUrl = "https://blockfade.com/api/forgotPassword/update";
        const bearerHeader = 'Basic ' + this.props.resetToken

        //Send request to API
        fetch(requestUrl, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': bearerHeader,
            },
            body: JSON.stringify({passwordVal})
        })
            .then(response => response.json())
            .then(response => {
                //If error throw to catch
                if (response.error) {
                    throw response
                }

                return this.setState({
                    sendingRequest: false,
                    error: false,
                    message: 'We have updated your password!',
                    passwordResetSuccess: true,
                })
            })
            .catch(error => {
                this.setState({
                    sendingRequest: false,
                    error: true,
                    message: error.error,
                })
            })
    }

    componentDidMount() {
        //Check if token is valid
        const resetToken = this.props.resetToken;
        this.validateResetToken(resetToken)
    }

    render() {
        //If the token is invalid show the error message
        if (this.state.invalidToken) {
            return (
                <div>
                    <img className="forgotPasswordIcon" src="/padlock.png" alt="padlock" />
                    <div className="forgotPasswordHeading">Oops, looks like this won't work</div>
                    <div className="forgotPasswordResetMessage">This link is no longer valid and has either expired or been used. If you would like to reset your password please request another email.</div>
                </div>
            )
        }

        //Password inputs
        const forgotPasswordInputs = (
            <div>
                <input onChange={this.handlePasswordChange} onBlur={this.checkPasswordLength} type="password" className="forgotPasswordInput" placeholder="New Password" />
                <input onChange={this.handleConfirmPasswordChange} type="password" className="forgotPasswordInput" style={{ marginTop:"5px" }} placeholder="Confirm Password" />
                <button onClick={this.handleClick} disabled={this.state.sendingRequest} className="forgotPasswordButton">
                    {this.state.sendingRequest ? <Facebook size={24} color={"#FFFFFF"} /> : "Save new password" }
                </button>
            </div>
        )

        return (
            <div>
                <img className="forgotPasswordIcon" src="/padlock.png" alt="padlock" />
                <div className="forgotPasswordHeading">Reset your password</div>
                <div className="forgotPasswordResetMessage">Enter your new password and confirm it to make sure it's correct</div>

                <div className={this.state.error ? 'forgotPasswordMessage errorColor' : 'forgotPasswordMessage successColor'}>{this.state.message}</div> 
                
                <div className="forgotPasswordBody">
                    {this.state.passwordResetSuccess ? null : forgotPasswordInputs}
                </div>
            </div>
        )
    }
}