import React from 'react';
import { isEmpty, isEmail } from 'validator';
import { Facebook } from 'react-spinners-css';

import './forgotPassword.css'

export class ForgotPasswordSend extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            emailValue: '',
            sendingRequest: false,
            resetSuccess: false,
            error: false,
            message: '',
        }
    }

    //Update emailValue state when user enters email
    handleEmailChange = event => {
        const emailValue = event.target.value
        this.setState({ emailValue })
    }

    handleClick = () => {
        //To show loading indicator
        this.setState({
            sendingRequest: true,
            message: '',
        });

        const emailValue = this.state.emailValue
        
        //Check if empty
        if (isEmpty(emailValue)) {
            return this.setState({
                sendingRequest: false,
                error: true,
                message: "Email cannot be empty"
            });
        }

        //Check if valid format
        if (!isEmail(emailValue)) {
            return this.setState({
                sendingRequest: false,
                error: true,
                message: "Email is not a valid format"
            });
        }

        const requestUrl = "https://blockfade.com/api/forgotPassword/create?email=" + emailValue;

        fetch(requestUrl, {
            method: "POST"
        })
            .then(response => response.json())
            .then(response => {
                if (response.error) {
                    return this.setState({
                        error: true,
                        message: response.reason,
                        sendingRequest: false,
                    })
                }

                //Successful password reset
                return this.setState({
                    message: response.success,
                    sendingRequest: false,
                    resetSuccess: true,
                    error: false,
                    message: 'An email has been sent to your inbox for you to change your password'
                })
            })
            .catch(error => {
                return this.setState({
                    error: true,
                    message: error.reason,
                    sendingRequest: false,
                })
            })

    }

    render() {

        //Send reset email Input and button
        const forgotPasswordInputs = (
            <div>
                <input onChange={this.handleEmailChange} type="text" className="forgotPasswordInput" placeholder="Email address" />
                <button onClick={this.handleClick} disabled={this.state.sendingRequest} className="forgotPasswordButton">
                    {this.state.sendingRequest ? <Facebook size={24} color={"#FFFFFF"} /> : "Reset Password" }
                </button>
            </div>
        )

        return (
            <div>
                <img className="forgotPasswordIcon" src="/padlock.png" alt="padlock" />
                <div className="forgotPasswordHeading">Forgot your password?</div>
                <div className="forgotPasswordResetMessage">Enter your email address and we will send a reset email</div>

                <div className={this.state.error ? 'forgotPasswordMessage errorColor' : 'forgotPasswordMessage successColor'}>{this.state.message}</div> 
                
                <div className="forgotPasswordBody">
                    {this.state.resetSuccess ? null : forgotPasswordInputs}
                </div>
            </div>
        )
    }
}