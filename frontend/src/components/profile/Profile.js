import React from 'react';
import './profile.css';

export class Profile extends React.Component {

    render() {
        if (this.props.hasAuth) {
            const userDetails = this.props.userDetails
            const { email, firstName, lastName } = userDetails

            return (
                <div className='profileCard'>
                    <h3 className='profileTitle'>Profile details</h3>

                    <div className="profileCardDetails">

                        <div>
                            <img className="profileCardPicture" src="/user.png" alt="user" />
                        </div>

                        <div>
                            <div className='profileItem'>
                                <div className="profileItemLabel">Name</div>
                                {firstName} {lastName}
                            </div>

                            <div className='profileItem'>
                                <div className="profileItemLabel">Email</div>
                                {email}
                            </div>
                        </div>

                    </div>
                </div>
            )
        } else {
            //Not logged in
            return (
                <div>
                    <h1>Not logged in</h1>
                </div>
            )
        }
    }
}