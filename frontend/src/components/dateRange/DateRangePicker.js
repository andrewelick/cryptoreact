import React from 'react';
import './dateRange.css'

export class DateRangePicker extends React.Component {
    constructor(props) {
        super(props)

        this.handleClick = this.handleClick.bind(this)

        this.timeOptions = ['1h', '24h', '7d', '30d', '1y']

        this.state = {
            activeTime: '24h'
        }
    }

    handleClick(event) {
        const timeVal = event.target.value;

        //Change time for chart
        this.props.changeTime(timeVal)

        //Set active time to selected
        this.setState({ activeTime: timeVal })
    }

    render() {

        return (
            <div className="dateRangeList">
                {
                    this.timeOptions.map(time => {
                        return time === this.state.activeTime ? <button key={time} value={time} onClick={this.handleClick} className="dateRangeOption activeOption">{time}</button> : <button key={time} value={time} onClick={this.handleClick} className="dateRangeOption">{time}</button>

                    })
                }
            </div>
        )
    }
}