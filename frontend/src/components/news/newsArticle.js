import React from 'react';
import './news.css'
import moment from 'moment';

const NewsArticle = props => {
    const { id, title, body, imageurl, published_on, url } = props.article
    const source = props.article.source_info.name

    let formattedDate = moment.unix(published_on).format("MMMM Do, YYYY")

    return (
        <div key={id} className="newsArticleContainer">

            <a href={url} target="_blank" rel="noopener noreferrer"><img src={imageurl} className="newsArticleImage" alt="news article" /></a>

            <div>
                <div className="newsArticleTitle">
                    <a href={url} target="_blank" rel="noopener noreferrer">{title}</a>
                </div>

                <div className="newsArticleDetails">
                    <span className="newsArticleSource">{source}</span>
                    <span className="newsArticleDate">{formattedDate}</span>
                </div>

                <div className="newsArticleBody">{body}</div>
            </div>

        </div>
    )
}

export { NewsArticle };