import React, { useEffect, useState } from 'react';
import { NewsArticle } from './newsArticle'
import { Facebook } from 'react-spinners-css';
import './news.css';

const News = props => {
    let abortController = new AbortController();
    const [ loadingStories, updateLoadingStories ] = useState(true);
    const [ stories, updateStories ] = useState([]);
    const [ loadingError, updateLoadingError ] = useState(false);

    //Get crypto news from CryptoCompare
    const getNewsFeed = () => {
        let apiUrl

        //Specific coin news feed
        if (props.coinSymbol) {
            const { coinSymbol, coinName } = props;
            const categories = coinSymbol + "," + coinName
            apiUrl = "https://min-api.cryptocompare.com/data/v2/news/?lang=EN&sortOrder=popular&categories=" + categories + "api_key="
        } else {
            apiUrl = "https://min-api.cryptocompare.com/data/v2/news/?lang=EN&api_key="
        }
        
        //URL to our API
        const url = "https://blockfade.com/api/news"
        
        fetch(url, {
            signal: abortController.signal,
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ apiUrl })
        })
            .then(response => response.json())
            .then(news => {
                news.Data.map(story => {
                    return updateStories(oldArray => [...oldArray, story]);
                });
            })
            .then(() => {
                //Hide loading indicator
                updateLoadingStories(false);
            })
            .catch(error => {
                updateLoadingError(true);
                console.log(error)
            });
    }

    useEffect(() => {
        //Fetch news stories and add to state
        getNewsFeed();
    }, []);

    //Loading stories
    if (loadingStories) return <Facebook color='#FFFFFF' />

    //Error occurred while loading news stories
    if (loadingError) {
        return "Error while loading news";
    }

    //Render news stories
    const amount = (props.storiesAmount) ? props.storiesAmount : 20;
    
    return stories.slice(0, amount).map(story => {
        return <NewsArticle key={story.id} article={story} />
    });
}

export { News };