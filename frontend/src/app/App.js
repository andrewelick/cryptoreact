import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.css';

//Authentication
import { checkUserStorage, authCheck, updateAccessToken, signOutUser } from './Auth';

//Header and Footer
import { Header } from '../components/header/header';
import { Footer } from '../components/footer/footer';

//Pages
import { HomePage } from '../pages/homepage';
import { LoginPage } from '../pages/loginPage';
import { SignUpPage } from '../pages/signUpPage';
import { PricesPage } from '../pages/pricesPage';
import { NewsPage } from '../pages/newsPage';
import { ProfilePage } from '../pages/profilePage';
import { CoinPage } from '../pages/coinPage';
import { ConfirmEmailPage } from '../pages/confirmEmailPage';
import { EmailNotConfirmedPage } from '../pages/emailNotConfirmedPage';
import { ForgotPasswordPage } from '../pages/forgotPasswordPage';
import { ContactUsPage } from '../pages/contactUsPage';

class App extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            authStatus: {
                hasAuth: false,
            }
        }
    }

    //Check Authentication
    handleAuth = async () => {
        const hasStorage = checkUserStorage();

        //If user not storage
        if (!hasStorage) {
            return { hasAuth: false }
        }

        //Check access token
        const accessToken = hasStorage.tokens.accessToken;
        const validAccessToken = await authCheck(accessToken);

        //If access token is valid
        if (validAccessToken.success) {
            return {
                hasAuth: true,
                userDetails: hasStorage
            }
        }

        //If access token is not valid
        if (validAccessToken.error) {
            const reason = validAccessToken.reason

            //If access token expired
            if (reason === 'expired') {
                //Refresh access token
                const refreshToken = hasStorage.tokens.refreshToken
                const tokenRefreshed = await updateAccessToken(refreshToken)

                //If successful refresh
                if (tokenRefreshed.accessToken) {
                    const newAccessToken = tokenRefreshed.accessToken

                    //Update local storage
                    hasStorage.tokens.accessToken = newAccessToken
                    localStorage.setItem('userDetails', JSON.stringify(hasStorage))

                    //Set new state
                    return {
                        hasAuth: true,
                        userDetails: hasStorage
                    }
                }

                //If refresh failed
                console.log(tokenRefreshed)

                return { hasAuth: false }
            }

            //If access token invalid other than for being expired
            console.log(validAccessToken)

            return { hasAuth: false }
        }
    }

    //Update auth status
    checkAuthStatus = () => {
        this.handleAuth()
            .then(authState => {
                return this.setState({ authStatus: authState })
            })
            .catch(error => {
                console.log("There was an error checking the status of user authentication")
                console.log(error)
            })
    }

    //Sign user out
    signOutUser = async () => {
        const hasStorage = checkUserStorage();

        //If user not storage
        if (!hasStorage) {
            return { hasAuth: false }
        }

        //Delete refresh token
        const refreshToken = hasStorage.tokens.refreshToken
        const userSignedOut = await signOutUser(refreshToken);

        //If error
        if (userSignedOut.error) {
            const error = userSignedOut.error
            console.log(error)
            return { error: error }
        }

        this.setState({ authStatus: { hasAuth: false } })
        return userSignedOut
    }

    componentDidMount() {
        this.checkAuthStatus()
    }

    render() {
        //User logged in or not state
        const hasAuth = this.state.authStatus.hasAuth
        
        //If user logged in
        if (hasAuth) {
            //If user has confirmed their email or not
            const emailConfirmed = this.state.authStatus.userDetails.emailConfirmed
            
            if (!emailConfirmed) {
                //Route only to confirm email page
                return (
                    <div className="appPage" >
                        <Router>
                            <Header authStatus={this.state.authStatus} signOutUser={this.signOutUser} />
        
                            <div className="pageBody">
                                <Route path="/" render={(props) => <EmailNotConfirmedPage {...props} authStatus={this.state.authStatus} />} />
                            </div>
        
                            <Footer />
                        </Router>
                    </div>
                );
            }
        }

        return (
            <div className="appPage" >
                <Router>
                    <Header authStatus={this.state.authStatus} signOutUser={this.signOutUser} />

                    <div className="pageBody">
                        <Route path="/login" component={(props) => <LoginPage {...props} authStatus={this.state.authStatus} handleAuth={this.checkAuthStatus} />} />
                        <Route path="/forgot" component={(props) => <ForgotPasswordPage {...props} authStatus={this.state.authStatus} />} />
                        <Route path="/signup" component={(props) => <SignUpPage {...props} authStatus={this.state.authStatus} handleAuth={this.checkAuthStatus} />} />
                        <Route path="/magic/confirmAccount" component={ConfirmEmailPage} />
                        <Route path="/news" component={NewsPage} />
                        <Route path="/contact" component={ContactUsPage} />
                        <Route path="/coin/:coinId" component={(props) => <CoinPage {...props} />} />
                        <Route path="/prices" component={PricesPage} />
                        <Route path="/profile" render={(props) => <ProfilePage {...props} authStatus={this.state.authStatus} />} />
                        <Route path="/" exact render={(props) => <HomePage {...props} authStatus={this.state.authStatus} />} />
                    </div>

                    <Footer />
                </Router>
            </div>
        );
    }
}

export default App;
