//Send API request to Auth serv
const sendAPIRequest = (url, method, headerAuth, bodyData, callback) => {
    const headers = { 'Content-Type': 'application/json' }
    const body = {}

    //Assign auth header
    if (headerAuth) {
        headers.Authorization = headerAuth
    }

    //Assign body data
    if (bodyData) {
        Object.keys(bodyData).map(key => {
            return body[key] = bodyData[key]
        })
    }

    //Send request to auth endpoint
    fetch(url, {
        'method': method,
        'headers': headers,
        'body': JSON.stringify(body),
    })
        .then(response => response.json())
        .then(response => {
            return callback(null, response)
        })
        .catch(error => {
            return callback(error, null)
        });
}

//Check local storage for user
exports.checkUserStorage = () => {
    try {
        //Get user details from localStorage
        const userAuth = JSON.parse(localStorage.getItem('userDetails'))

        //If userAuth does not exist
        if (!userAuth) return null

        return userAuth

    } catch {
        localStorage.clear()
        return null
    }
}

//Check if user already has creds
exports.authCheck = accessToken => {
    return new Promise((resolve, reject) => {
        const method = 'post'
        const url = "https://blockfade.com/api/token/check"
        const bearerHeader = 'Basic ' + accessToken

        //Send request to auth endpoint
        sendAPIRequest(url, method, bearerHeader, null, (err, res) => {
            if (err) {
                reject(err)
            }

            resolve(res)
        })
    });
}

//Refresh access token
exports.updateAccessToken = refreshToken => {
    return new Promise((resolve, reject) => {
        const method = 'post'
        const url = "https://blockfade.com/api/token/refresh"
        const bodyData = { refreshToken: refreshToken }

        //Send request to auth endpoint
        sendAPIRequest(url, method, null, bodyData, (err, res) => {
            if (err) {
                reject(err)
            }

            //Set new access token
            const newAccessToken = res.accessToken
            resolve({ accessToken: newAccessToken })
        });
    });
}

//Sign out user
exports.signOutUser = refreshToken => {
    return new Promise((resolve, reject) => {
        const method = "delete"
        const url = "https://blockfade.com/api/user/logout"
        const bodyData = { refreshToken: refreshToken }

        sendAPIRequest(url, method, null, bodyData, (err, res) => {
            if (err) {
                reject(err)
            }

            //Delete user from local storage
            localStorage.removeItem('userDetails')

            //Send confirmation of deletion
            resolve(res)
        })
    })
}
