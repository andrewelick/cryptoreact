# BlockFade application
This is the codebase for the [BlockFade](https://www.blockfade.com)  wesbite. It is running on a React powered Frontend and the Node.js powered Backend-API.  It is being hosted on Linode and all user data is stored in a PostgreSQL database on AWS RDS.

# Environment Setup

To get up and running to begin developing and testing please follow the steps listed below.

## Clone this repository

Clone this repository on your local machine either using SSH or HTTPS.
`git@gitlab.com:USERNAME/cryptoreact.git`

*If you are using SSH please add the public key from your local machine on to your account.*

## Install package dependencies

Using a terminal shell navigate to the Frontend folder and then run:
`npm install`

Then navigate to the node-app folder located within the Backend folder and run:
`npm install`

## Create environment file

Navigate to the /backend/node-app folder and create a file titled: `.env`

Next add the following lines to the file and then fill in the correct information to those fields.

```
DB_HOST=
DB_PORT=
DB_DATABASE=
DB_USER=
DB_PASSWORD=

JWT_ACCESS_TOKEN_SECRET=
JWT_REFRESH_TOKEN_SECRET=

CRYPTO_COMPARE_API_KEY=

EMAIL_SENDER_API_KEY=
EMAIL_SENDER_DOMAIN=
```
